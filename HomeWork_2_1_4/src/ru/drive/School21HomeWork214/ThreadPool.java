package ru.drive.School21HomeWork214;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {

    private PoolWorker[] threads;
    private Deque<Runnable> tasks;
    private Boolean endThreads;

    private class PoolWorker extends Thread {
        @Override
        public void run() {

            Runnable runnable;
            while (true) {

                synchronized (tasks) {

                    if (tasks.size() == 0) {

                        if (endThreads) {
                            break;
                        }
                        continue;
                    }

                    runnable = tasks.getFirst();
                    tasks.pollFirst();
                    tasks.notify();

                }

                if (runnable == null) {
                    continue;
                }

                try {
                    //Мало ли что написано в коде runnable
                    runnable.run();

                } catch (Exception e) {
                    //В executorService ничего не сообщается и поток не прерывается
                    //throw new IllegalArgumentException(e);
                }

            }
        }
    }

    public ThreadPool(int threadsCount) {
        this.endThreads = false;
        this.tasks = new LinkedList<>();
        this.threads = new PoolWorker[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            this.threads[i] = new PoolWorker();
            this.threads[i].start();
        }
    }

    public void submit(Runnable task) {

        if (endThreads) {
            return;
        }

        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    public void shutdown() {
        //ждем, чтобы то что выполнялось выполнилось.
        //но в run цикл прерываем. Т.е. больше не принимаем новых задач
        this.endThreads = true;
    }
}

