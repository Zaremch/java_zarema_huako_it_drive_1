package ru.drive.School21HomeWork214;

public class Main {

    public static void main(String[] args) {

        ThreadPool threadPool = new ThreadPool(2);
        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " A");
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " B");
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " C");
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println("А если плохой код тут: " + 1/0);
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " B1");
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " C1");
            }
        });

        threadPool.shutdown();

        threadPool.submit(() -> {
            for (int i = 0; i < 30; i++) {
                System.out.println("Облом!");
            }
        });

    }
}
