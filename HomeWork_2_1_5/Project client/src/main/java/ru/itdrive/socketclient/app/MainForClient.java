package ru.itdrive.socketclient.app;
import ru.itdrive.socketclient.utils.SocketClient;
import ru.itdrive.socketclient.utils.Arguments;
import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        System.out.println(arguments.serverPort);
        System.out.println(arguments.serverHost);

        SocketClient client = new SocketClient(arguments.serverHost, arguments.serverPort);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}