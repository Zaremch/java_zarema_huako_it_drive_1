package ru.itdrive.socketserver.app;

import ru.itdrive.socketserver.utils.EchoServerSocket;
import ru.itdrive.socketserver.utils.Arguments;
import com.beust.jcommander.JCommander;
import java.net.ServerSocket;

public class MainForServer {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        System.out.println(arguments.port);

        EchoServerSocket serverSocket = new EchoServerSocket();
        serverSocket.start(arguments.port);
    }
}