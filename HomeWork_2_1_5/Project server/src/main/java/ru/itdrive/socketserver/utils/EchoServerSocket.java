package ru.itdrive.socketserver.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class EchoServerSocket {

    LinkedList<Socket> listSocket = new LinkedList();

    private class ReceiverMessageTaskClient extends Thread {

        private Socket socketClient;

        public ReceiverMessageTaskClient(Socket socketClient) {
            this.socketClient = socketClient;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    System.out.printf("На сервере запущен фоновый процесс: %s \n", Thread.currentThread().getName());

                    InputStream clientInputStream = socketClient.getInputStream();
                    BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));

                    String inputLine;
                    while (true) {
                        inputLine = clientReader.readLine();
                        for (Socket сurrentSocket : listSocket) {
                            PrintWriter writer = new PrintWriter(сurrentSocket.getOutputStream(), true);
                            writer.println(inputLine);
                        }
                        System.out.println(inputLine);
                    }

                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    ;

    public void start(int port) {

        ServerSocket serverSocket;

        try {

            serverSocket = new ServerSocket(port);

            while (true) {
                Socket socketClient = serverSocket.accept();
                listSocket.add(socketClient);
                ReceiverMessageTaskClient receiverMessageTaskClient = new ReceiverMessageTaskClient(socketClient);
                receiverMessageTaskClient.start();
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

    }
}