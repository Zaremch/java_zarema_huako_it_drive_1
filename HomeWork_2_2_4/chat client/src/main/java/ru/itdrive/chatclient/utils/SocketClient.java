package ru.itdrive.chatclient.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {
    private Socket client;
    private PrintWriter toServer;
    private BufferedReader fromServer;

    private Runnable receiverMessageTask = new Runnable() {
        public void run() {
            while (true) {
                try {
                    String messageFromServer = fromServer.readLine();
                    if (messageFromServer != null) {
                        System.out.println(messageFromServer);
                    }
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };

    public SocketClient(String host, int port) {
        try {
            client = new Socket(host, port);
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessageTask).start();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(String massage) {
        toServer.println(massage);
    }
}
