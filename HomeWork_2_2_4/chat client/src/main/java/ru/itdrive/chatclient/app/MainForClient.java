package ru.itdrive.chatclient.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.chatclient.utils.SocketClient;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        SocketClient client = new SocketClient(arguments.serverHost, arguments.serverPort);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose nickname:");

        String nickname = scanner.nextLine();
        client.sendMessage("Choose nickname <" + nickname + ">");

        System.out.println("________________________________");
        System.out.println("Command list: ");
        System.out.println(" - Choose room <room>");
        System.out.println(" - Exit from room");
        System.out.println("________________________________");

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }

    }
}
