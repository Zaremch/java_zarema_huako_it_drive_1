create table room
(
    id    serial primary key,
    title char(100)
);

create table users
(
    id       serial primary key,
    nickname char(100)
);


create table message
(
    id           serial primary key,
    text         char(1000),
    date_of_post timestamp,
    room_id      integer,
    author_id    integer,
    foreign key (room_id) references room (id),
    foreign key (author_id) references users (id)
);

create table user_rooms
(
    room_id integer,
    user_id integer,
    foreign key (room_id) references room (id),
    foreign key (user_id) references users (id)
);

insert into room (title)
values ('Первая комната');

select *
from room;

Select *
from room
where title = 'Первая комната';

SELECT *
from users
where nickname = 'Helga';

INSERT into users (nickname)
values ('Helga');
INSERT into users (nickname)
values ('User1');

insert into message(text, date_of_post, room_id, author_id)
values ('ТекстищеФЫВ', '2020_07_23 10:00:00', 1, 1);

insert into message(text, date_of_post, room_id, author_id)
values ('QQQQQQ', '2020_04_22', 1, 1);

insert into message(text, date_of_post, room_id, author_id)
values ('Текстище0', '2020_01_22', 1, 2);

SELECT message.id,
       message.date_of_post,
       message.text,
       message.room_id,
       r.title,
       message.author_id,
       u.nickname
from message
         inner join users u on u.id = message.author_id
         inner join room r on r.id = message.room_id
where room_id = 4
order by date_of_post
limit 2;

select * from users;

insert into user_rooms(room_id, user_id) values (1,1);
select * from user_rooms;

delete from user_rooms where room_id =1 and user_id = 1;

INSERT INTO user_rooms (room_id, user_id)
SELECT * FROM (SELECT 1, 1) AS tmp
WHERE NOT EXISTS (
        SELECT user_id FROM user_rooms WHERE room_id = 1 and user_id = 1
    ) LIMIT 1;

select * from user_rooms;

create table room_visit (date_visit timestamp,
    room_id integer,
    user_id integer,
    foreign key (room_id) references room (id),
    foreign key (user_id) references users (id)
);

select * from room_visit;

select
r.room_id,
       r2.title
from room_visit as r
inner join room r2 on r2.id = r.room_id
where r.user_id = 12 and date_visit =(
SELECT max(date_visit) from room_visit where user_id = r.user_id)



