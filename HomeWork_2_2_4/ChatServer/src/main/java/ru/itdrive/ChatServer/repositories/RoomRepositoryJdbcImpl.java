package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

public class RoomRepositoryJdbcImpl implements RoomRepository {
    private DataSource dataSource;

    public RoomRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Room save(Room room) {

        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            Connection connection = this.dataSource.getConnection();

            //language=SQL
            String sql_text = "INSERT into room (title) values (?);";

            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, room.getTitle());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("Room save not execute");
            }

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                room.setId(resultSet.getInt("id"));
            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return room;
    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Room object) {

    }

    @Override
    public Room findByRoom(Room room) {

        //language=SQL
        String sql_text = "Select * from room where title = '%title%'";
        sql_text = sql_text.replace("%title%", room.getTitle());

        Statement statement = null;
        ResultSet resultSet = null;

        //
        Connection connection = null;
        try {

            //Вот тут иногда падает. Спросить
            //Connection is not available, request timed out after 30003ms.
            connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                room.setId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        System.out.println("Вася1238");
        return room;
    }

    @Override
    public Room findByLastUserVisit(User user) {

        //language=SQLroom_visit
        String sql_text = "select\n" +
                "r.room_id as id,\n" +
                "       r2.title as title\n" +
                "from room_visit as r\n" +
                "inner join room r2 on r2.id = r.room_id\n" +
                "where r.user_id = %user_id% and date_visit =(\n" +
                "SELECT max(date_visit) from room_visit where user_id = r.user_id)";

        sql_text = sql_text.replace("%user_id%", "" + user.getId());

        Statement statement = null;
        ResultSet resultSet = null;
        Room room = null;

        try {

            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {

                room = Room.builder()
                        .id(resultSet.getInt("id"))
                        .title(resultSet.getString("title").trim())
                        .build();

            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return room;
    }

    @Override
    public Room find(Integer id) {

        //language=SQL
        String sql_text = "Select * from room where id = '%id%'";
        sql_text = sql_text.replace("%id%", "" + id);

        Room room = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                room = Room.builder()
                        .id(resultSet.getInt("id"))
                        .title(resultSet.getString("title"))
                        .build();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return room;
    }

    @Override
    public List<Room> findALL() {
        return null;
    }
}
