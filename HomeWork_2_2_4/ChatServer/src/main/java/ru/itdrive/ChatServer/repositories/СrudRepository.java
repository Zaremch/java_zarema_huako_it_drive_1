package ru.itdrive.ChatServer.repositories;

import java.util.List;

public interface СrudRepository<T> {
    T save(T object);

    void update(T object);

    void delete(T object);

    T find(Integer id);

    List<T> findALL();
}
