package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.List;

public class UserRepositoryJdbcImpl implements UserRepository {

    private DataSource dataSource;

    public UserRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public User save(User user) {

        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            //language=SQL
            String sql_text = "INSERT into users (nickname) values (?);";

            Connection connection = this.dataSource.getConnection();
            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getNickname());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("User save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Integer id = generatedKeys.getInt("id");
                user.setId(id);
            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public void update(User object) {

    }

    @Override
    public void delete(User object) {

    }

    @Override
    public User findByUser(User user) {

        //language=SQL
        String sql_text = "SELECT * from users where nickname = '%nickname%';";
        sql_text = sql_text.replace("%nickname%", user.getNickname());

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public void saveToRoom(User user, Room room) {

        //language=SQL
        String sql_text = "INSERT INTO user_rooms (room_id, user_id)\n" +
                "SELECT * FROM (SELECT ?, ?) AS tmp\n" +
                "WHERE NOT EXISTS (\n" +
                "        SELECT user_id FROM user_rooms WHERE room_id = ? and user_id = ?\n" +
                "    ) LIMIT 1;\n" +
                "insert into room_visit (date_visit, room_id, user_id) \n" +
                "values (?, ?, ?);";

        PreparedStatement statement = null;

        try {

            Integer idUser = user.getId();
            Integer idRoom = room.getId();

            Connection connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(sql_text);
            statement.setInt(1, idRoom);
            statement.setInt(2, idUser);
            statement.setInt(3, idRoom);
            statement.setInt(4, idUser);

            System.out.println("Вася11");
            java.util.Date date = new Date();
            statement.setTimestamp(5, new Timestamp(date.getTime()));

            System.out.println("Вася12");

            statement.setInt(6, idRoom);
            statement.setInt(7, idUser);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

        }
    }

    @Override
    public User find(Integer id) {

        //language=SQL
        String sql_text = "SELECT * from users where id = '%id%';";
        sql_text = sql_text.replace("%id%", "" + id);

        User user = null;

        Statement statement = null;
        ResultSet resultSet = null;

        try {
            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                user = User.builder()
                        .id(resultSet.getInt("id"))
                        .nickname(resultSet.getString("nickname").trim())
                        .build();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public List<User> findALL() {
        return null;
    }
}
