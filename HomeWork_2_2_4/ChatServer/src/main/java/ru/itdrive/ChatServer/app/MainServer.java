package ru.itdrive.ChatServer.app;

import com.beust.jcommander.JCommander;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.ChatServer.services.SocketServer;
import ru.itdrive.ChatServer.services.SocketServerImpl;

public class MainServer {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        SocketServer service = context.getBean(SocketServerImpl.class);
        service.start(arguments.port);
        //service.start(7777);

    }
}
