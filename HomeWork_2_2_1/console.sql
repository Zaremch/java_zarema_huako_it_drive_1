
drop table message;
drop table user_chats;
drop table chat;
drop table users;

create table users (id serial primary key,
first_name char(100), last_name char(100), email char(100), password char(100))
select * from users;
insert into users (first_name, last_name, email, password) VALUES
('Zarema', 'Khuako', 'mmm@mail.ru', '123');
insert into users (first_name, last_name, email, password) VALUES
('Vasia', 'Pupkin', 'mmm2@mail.ru', '123');

create table chat (id serial primary key, name char(100),
date_of_creation timestamp);
select * from chat;
insert into chat (name, date_of_creation) values ('Java', '2020_08_21');
insert into chat (name, date_of_creation) values ('Travels', '2020_08_21');

create table message(id serial primary key, text char(1000), author_id integer, chat_id integer,
foreign key (author_id) references users (id),
foreign key (chat_id) references chat (id)
);
select * from message;
insert into message (text, author_id, chat_id) values ('Текстище', 1, 1);

create table user_chats (user_id integer, chat_id integer,
foreign key (user_id) references users (id),
foreign key (chat_id) references chat (id)
);
insert into user_chats (user_id, chat_id) values (1,1);
select * from user_chats;

