package com.company;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static final String FILE_DICTOINARY = "resources\\dictionary.txt";

    public static void main(String[] args) {

        Date dateStart = new Date();
        System.out.println(dateStart.toString());

        Scanner scanner = new Scanner(System.in);
        String fileName1 = scanner.next();
        String fileName2 = scanner.next();

        FileAnalysis fileAnalysis = new FileAnalysis(fileName1, fileName2);
        ArrayList<String> dictionary = fileAnalysis.getDictionary();

        try (BufferedWriter writter = new BufferedWriter(new FileWriter(FILE_DICTOINARY))) {
            for (String word : dictionary) {
                writter.write(word + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.printf("Результат схожести (косинусная мера): %.2f", fileAnalysis.сosineSimilarity());
        System.out.println();

        Date dateEnd = new Date();
        System.out.println(dateEnd.toString());

    }

}
