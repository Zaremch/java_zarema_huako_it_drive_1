package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileAnalysis {

    private ArrayList<Integer> vector1;
    private ArrayList<Integer> vector2;
    private ArrayList<String> dictionary;

    public FileAnalysis(String fileName1, String fileName2) {

        this.vector1 = new ArrayList<Integer>();
        this.vector2 = new ArrayList<Integer>();
        this.dictionary = new ArrayList<String>();

        readFile(fileName1, fileName2);

    }

    public ArrayList<String> getDictionary() {
        return dictionary;
    }

    public double сosineSimilarity() {

        int numerator = 0;
        double sumVector1 = 0;
        double sumVector2 = 0;

        for (int i = 0; i < dictionary.size(); i++) {
            numerator += vector1.get(i) * vector2.get(i);
            sumVector1 += vector1.get(i) * vector1.get(i);
            sumVector2 += vector2.get(i) * vector2.get(i);
        }
        double denominator = Math.sqrt(sumVector1) * Math.sqrt(sumVector2);

        if (denominator == 0) {
            return 0;
        } else {
            return numerator / denominator;
        }
    }

    private void readFile(String fileName1, String fileName2) {

        ArrayList<String> arrayAllWords1 = fileWords(fileName1);
        ArrayList<String> arrayAllWords2 = fileWords(fileName2);

        int length1 = arrayAllWords1.size();
        int length2 = arrayAllWords2.size();
        int length = length1 + length2;

        String currentWorld = "";
        String currentWorld2 = "";

        int count1, count2;
        for (int i = 0; i < length; i++) {

            if (i < length1) {
                currentWorld = arrayAllWords1.get(i);
                count1 = 1;
                count2 = 0;

            } else {
                currentWorld = arrayAllWords2.get(i - length1);
                count1 = 0;
                count2 = 1;
            }

            if (currentWorld == null) {
                continue;
            }

            for (int j = i + 1; j < length - 1; j++) {
                if (j < length1) {
                    currentWorld2 = arrayAllWords1.get(j);
                } else {
                    currentWorld2 = arrayAllWords2.get(j - length1);
                }

                if (currentWorld2 == null) {
                    continue;
                }

                if (currentWorld.equals(currentWorld2)) {
                    if (j < length1) {
                        arrayAllWords1.set(j, null);
                        count1++;
                    } else {
                        arrayAllWords2.set(j - length1, null);
                        count2++;
                    }
                }
            }
            dictionary.add(currentWorld);
            vector1.add(count1);
            vector2.add(count2);
        }

        int iMin;
        String temp;
        length = dictionary.size();
        int tempCount;
        for (int i = 0; i < length; i++) {
            iMin = i;
            for (int j = i + 1; j < length; j++) {
                if (stringCompare(dictionary.get(j), dictionary.get(iMin)) == -1) {
                    iMin = j;
                }
            }
            if (iMin != i) {
                temp = dictionary.get(iMin);
                dictionary.set(iMin, dictionary.get(i));
                dictionary.set(i, temp);

                tempCount = vector1.get(iMin);
                vector1.set(iMin, vector1.get(i));
                vector1.set(i, tempCount);

                tempCount = vector2.get(iMin);
                vector2.set(iMin, vector2.get(i));
                vector2.set(i, tempCount);
            }
        }

    }

    private ArrayList<String> fileWords(String fileName) {

        ArrayList<String> fileWords = new ArrayList<String>();

        try {

            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            // чтение посимвольно
            int i;
            String world = "";
            while ((i = bufferedReader.read()) != -1) {

                if (isPartWord(i)) {
                    world = world + (char) i;
                } else if (!world.equals("")) {
                    fileWords.add(world);
                    world = "";
                }

            }
            if (!world.equals("")) {
                fileWords.add(world);
            }

            bufferedReader.close();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return fileWords;
    }

    private boolean isPartWord(int index) {

        if (index == 45) {
            return true;
        }

        if (index >= 65 && index <= 90) {
            return true;
        }

        if (index >= 97 && index <= 122) {
            return true;
        }

        if (index >= 1040 && index <= 1103) {
            return true;
        }

        return false;
    }

    public static int stringCompare(String line1, String line2) {

        char[] arrayСharacter1 = line1.toCharArray();
        char[] arrayСharacter2 = line2.toCharArray();

        int lengthArrayСharacter1 = arrayСharacter1.length;
        int lengthArrayСharacter2 = arrayСharacter2.length;

        int minlength = Math.min(lengthArrayСharacter1, lengthArrayСharacter2);
        int result = 0;
        if (lengthArrayСharacter1 < lengthArrayСharacter2) {
            result = -1;
        } else if (lengthArrayСharacter1 > lengthArrayСharacter2) {
            result = 1;
        }

        for (int i = 0; i < minlength; i++) {
            if (arrayСharacter1[i] < arrayСharacter2[i]) {
                result = -1;
                break;
            }

            if (arrayСharacter1[i] > arrayСharacter2[i]) {
                result = 1;
                break;
            }
        }

        return result;
    }
}
