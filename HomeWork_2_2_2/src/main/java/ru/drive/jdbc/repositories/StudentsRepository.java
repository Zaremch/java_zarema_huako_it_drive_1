package ru.drive.jdbc.repositories;

import ru.drive.jdbc.models.Student;

public interface StudentsRepository extends GrudRepository<Student>{
    Student findByFirstName(String firstName);
}