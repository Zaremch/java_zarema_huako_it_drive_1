package ru.drive.jdbc.repositories;

import ru.drive.jdbc.models.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    private Connection connection;

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from student";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";

    private RowMapper<Student> studentRowMapper = new RowMapper() {
        @Override
        public Student mapRow(ResultSet row) throws SQLException {
            return new Student(row.getInt("id"),
                    row.getString("first_name"),
                    row.getString("last_name"),
                    row.getInt("age"),
                    row.getBoolean("is_active"));
        };
    };

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Student findByFirstName(String firstName) {
        return null;
    }

    @Override
    public void save(Student object) {

    }

    @Override
    public void update(Student object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Student find(Integer id) {

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();

            return studentRowMapper.mapRow(resultSet);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<Student> findALL() {

        try {

            List<Student> result = new ArrayList<Student>();

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Student student = studentRowMapper.mapRow(resultSet);
                result.add(student);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
