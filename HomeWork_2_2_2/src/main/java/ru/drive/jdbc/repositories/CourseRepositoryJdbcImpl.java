package ru.drive.jdbc.repositories;

import ru.drive.jdbc.models.Course;
import ru.drive.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CourseRepositoryJdbcImpl implements CourseRepository {

    private Connection connection;

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {

            if (row.getInt("lesson_id") == -1) {
                return null;
            }

            return new Lesson(row.getInt("lesson_id"),
                    row.getString("name"));
        }
    };

    private RowMapper<Course> сourseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {

            return new Course(row.getInt("id"),
                    row.getString("title"),
                    row.getDate("start_date"),
                    row.getDate("finish_date"));
        }
    };

    public CourseRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Course object) {

    }

    @Override
    public void update(Course object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Course find(Integer id) {

        //language=SQL
        String sql_text = "select\n"
                + "course.id,\n"
                + "course.title,\n"
                + "course.start_date,\n"
                + "course.finish_date,\n"
                + "coalesce(lesson.id, -1) as lesson_id,\n"
                + "lesson.name\n"
                + "from course\n"
                + "left outer join lesson on course.id = lesson.course_id\n"
                + "where course.id = %id%;";

        sql_text = sql_text.replace("%id%", "" + id);

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);

            boolean first = true;
            Course course = null;
            while (resultSet.next()) {

                if (first) {
                    course = сourseRowMapper.mapRow(resultSet);
                    first = false;
                }

                Lesson lesson = lessonRowMapper.mapRow(resultSet);
                if (lesson != null) {
                    course.addLesson(lesson);
                    lesson.setCourse(course);
                }

            }

            return course;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<Course> findALL() {

        //language=SQL
        String sql_text = "select\n"
                + "course.id,\n"
                + "course.title,\n"
                + "course.start_date,\n"
                + "course.finish_date,\n"
                + "coalesce(lesson.id, -1) as lesson_id,\n"
                + "lesson.name\n"
                + "from course\n"
                + "left outer join lesson on course.id = lesson.course_id\n"
                + "order by course.id;";

        List<Course> result = new ArrayList<Course>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);

            Course currentCourse = null;
            Integer сourse_id;
            while (resultSet.next()) {

                сourse_id = resultSet.getInt("id");

                if (currentCourse == null) {
                    currentCourse = сourseRowMapper.mapRow(resultSet);
                    result.add(currentCourse);
                } else if (сourse_id != currentCourse.getId()) {
                    currentCourse = сourseRowMapper.mapRow(resultSet);
                    result.add(currentCourse);
                }

                Lesson lesson = lessonRowMapper.mapRow(resultSet);
                if (lesson != null) {
                    currentCourse.addLesson(lesson);
                    lesson.setCourse(currentCourse);
                }

            }

            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
