package ru.drive.jdbc.repositories;

import ru.drive.jdbc.models.Course;
import ru.drive.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonRepositoryJdbcImpl implements LessonRepository {

    private Connection connection;

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        @Override
        public Lesson mapRow(ResultSet row) throws SQLException {

            return new Lesson(row.getInt("id"),
                    row.getString("name"));
        }
    };

    private RowMapper<Course> сourseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet row) throws SQLException {

            if (row.getInt("course_id") == -1) {
                return null;
            }

            return new Course(row.getInt("course_id"),
                    row.getString("title"),
                    row.getDate("start_date"),
                    row.getDate("finish_date"));
        }
    };

    public LessonRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Lesson object) {

    }

    @Override
    public void update(Lesson object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Lesson find(Integer id) {

        //language=SQL
        String sql_text = ""
                + "select coalesce(lesson2.id, lesson.id) as id,\n"
                + "coalesce(lesson2.name, lesson.name) as name,\n"
                + "coalesce(lesson2.id, lesson.id) = %id% as main_id,\n"
                + "coalesce(course.id, -1) as course_id,\n"
                + "course.title,\n"
                + "course.start_date,\n"
                + "course.finish_date\n"
                + "from lesson\n"
                + "left outer join course on lesson.course_id = course.id\n"
                + "left outer join lesson as lesson2 on course.id = lesson2.course_id\n"
                + "where lesson.id = %id%\n"
                + "order by main_id desc;";

        sql_text = sql_text.replace("%id%", "" + id);

        Lesson mainLesson = null;

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);

            boolean first = true;
            Lesson currentLesson;
            Course course = null;

            List<Lesson> listLesson = new ArrayList<Lesson>();
            while (resultSet.next()) {

                if (first) {
                    mainLesson = lessonRowMapper.mapRow(resultSet);
                    course = сourseRowMapper.mapRow(resultSet);
                    mainLesson.setCourse(course);

                    listLesson.add(mainLesson);

                    first = false;

                } else {
                    currentLesson = lessonRowMapper.mapRow(resultSet);
                    currentLesson.setCourse(course);
                    listLesson.add(currentLesson);
                }

            }

            if (course != null) {
                course.setListLesson(listLesson);
            }

            return mainLesson;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }


    }

    @Override
    public List<Lesson> findALL() {

        //language=SQL
        String sql_text = "select\n"
                + "lesson.id as id,\n"
                + "lesson.name as name,\n"
                + "coalesce(lesson.course_id, -1) as course_id,\n"
                + "course.title,\n"
                + " course.start_date,\n"
                + "course.finish_date\n"
                + "from lesson left outer join course on lesson.course_id = course.id\n"
                + "order by lesson.course_id;";

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);

            List<Lesson> result = new ArrayList<Lesson>();

            Course currentCourse = null;
            Integer сourse_id;
            while (resultSet.next()) {

                сourse_id = resultSet.getInt("course_id");

                if (currentCourse == null) {
                    currentCourse = сourseRowMapper.mapRow(resultSet);
                } else if (сourse_id != currentCourse.getId()) {
                    currentCourse = сourseRowMapper.mapRow(resultSet);
                }

                Lesson lesson = lessonRowMapper.mapRow(resultSet);
                lesson.setCourse(currentCourse);

                result.add(lesson);

                if (currentCourse != null) {
                    currentCourse.addLesson(lesson);
                }
            }

            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
