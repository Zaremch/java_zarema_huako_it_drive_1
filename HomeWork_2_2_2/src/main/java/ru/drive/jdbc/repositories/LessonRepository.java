package ru.drive.jdbc.repositories;

import ru.drive.jdbc.models.Lesson;

public interface LessonRepository extends GrudRepository<Lesson> {

}
