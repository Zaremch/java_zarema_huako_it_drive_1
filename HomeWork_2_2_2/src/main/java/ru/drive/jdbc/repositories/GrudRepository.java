package ru.drive.jdbc.repositories;
import java.util.List;

public interface GrudRepository <T> {
    void save (T object);
    void update (T object);
    void delete(Integer id);
    T find(Integer id);
    List<T> findALL();
}
