package ru.drive.jdbc.models;

import java.util.*;

public class Course {

    private Integer id;
    private String title;
    private Date start_date;
    private Date finish_date;
    private List<Lesson> listLesson;

    public Course(Integer id, String title, Date start_date, Date finish_date) {
        this.id = id;
        this.title = title;
        this.start_date = start_date;
        this.finish_date = finish_date;
        this.listLesson = new ArrayList<Lesson>();
    }

    public void addLesson(Lesson lesson) {
        this.listLesson.add(lesson);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getStart_date() {
        return start_date;
    }

    public Date getFinish_date() {
        return finish_date;
    }

    public List<Lesson> getListLesson() {

        return listLesson;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setFinish_date(Date finish_date) {
        this.finish_date = finish_date;
    }

    public void setListLesson(List<Lesson> listLesson) {
        this.listLesson = listLesson;
    }

    @Override
    public String toString() {

        List<String> arrayLesson = new ArrayList<String>();
        for (Lesson lesson:listLesson) {
            arrayLesson.add(lesson.getName().trim());
        }

        String lessons = "-";
        if (arrayLesson.size() != 0) {
            lessons = arrayLesson.toString();
        }

        return "Course{" +
                "id=" + id +
                ", title='" + title.trim() + '\'' +
                ", start_date=" + start_date +
                ", finish_date=" + finish_date +
                ", listLesson=" + lessons +
                '}';
    }
}
