package ru.drive.jdbc.models;

public class Lesson {

    private Integer id;
    private String name;
    private Course course;

    public Lesson(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Course getCourse() {
        return course;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {

        return "Lesson{" +
                "id=" + id +
                ", name='" + name.trim() + '\'' +
                ", course=" + course +
                '}';
    }
}
