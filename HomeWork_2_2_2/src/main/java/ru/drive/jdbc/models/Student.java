package ru.drive.jdbc.models;

public class Student {
    private Integer id;
    private String first_name;
    private String last_name;
    private Integer age;
    private Boolean is_active;

    public Student(Integer id, String first_name, String last_name, Integer age, Boolean is_active) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
        this.is_active = is_active;
    }

    public Integer getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Integer getAge() {
        return age;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", first_name='" + first_name.trim() + '\'' +
                ", last_name='" + last_name.trim() + '\'' +
                ", age=" + age +
                ", is_active=" + is_active +
                '}';
    }
}
