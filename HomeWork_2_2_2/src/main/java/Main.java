import ru.drive.jdbc.models.Course;
import ru.drive.jdbc.models.Lesson;
import ru.drive.jdbc.models.Student;
import ru.drive.jdbc.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "QWE123qwe";

    public static void main(String[] args) throws Exception {

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        StudentsRepository repository = new StudentsRepositoryJdbcImpl(connection);

        Student student1 = repository.find(1);
        System.out.println(student1);
        System.out.println("_________________________");
        System.out.println(repository.findALL());

        System.out.println("_________________________");
        LessonRepository lessonRepository = new LessonRepositoryJdbcImpl(connection);
        Lesson lesson = lessonRepository.find(2);
        System.out.println(lesson);

        System.out.println("_________________________");
        List<Lesson> lessonList = lessonRepository.findALL();
        System.out.println(lessonList);


        CourseRepository courseRepository = new CourseRepositoryJdbcImpl(connection);

        System.out.println("********************************");
        Course course = courseRepository.find(1);
        System.out.println(course);

        System.out.println("********************************");
        List <Course> arrayCourse = new ArrayList<Course>();
        arrayCourse = courseRepository.findALL();
        System.out.println(arrayCourse);

    }
}
