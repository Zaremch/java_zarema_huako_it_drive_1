-- drop table student_course;
-- drop table lesson;
-- drop table course;
-- drop table student;

create table course (id serial primary key,
title char(100), start_date timestamp, finish_date timestamp);

create table lesson
(
    id        serial primary key,
    name      char(100),
    course_id integer,
    foreign key (course_id) references course (id)
);

select * from lesson;

create table student (id serial primary key,
first_name char(100),
last_name char(100),
age integer,
is_active bool);

select * from student;

create table student_course (student_id integer, course_id integer,
foreign key (student_id) references student (id),
foreign key (course_id) references course (id)
);

select * from student_course;

insert into student (first_name, last_name, age, is_active) values ('Zarema', 'Khuako', 29, true);

select lesson.id from lesson;

insert into course (title, start_date, finish_date) values ('Первый курс Прикладная математика', '2020_09_01', '2021_06_01');
insert into course (title, start_date, finish_date) values ('Первый курс Вышивание', '2020_09_01', '2021_06_01');
insert into course (title, start_date, finish_date) values ('Первый курс Медицина', '2020_09_01', '2021_06_01');

insert into course (title, start_date, finish_date) values ('Без урока', '2020_09_01', '2021_06_01');

select * from course;

insert into lesson (name) values ('Без курса');

select * from lesson;

insert into lesson (name, course_id) values ('Дискретная математика', 1);
insert into lesson (name, course_id) values ('Еще одна адская математика', 1);
insert into lesson (name, course_id) values ('Латынь', 3);

select coalesce(lesson2.id, lesson.id) as id,
       coalesce(lesson2.name, lesson.name) as name,
       coalesce(lesson2.id, lesson.id) = 2 as main_id,
       course.id as course_id,
       course.title,
       course.start_date,
       course.finish_date
from lesson
    left outer join course on lesson.course_id = course.id
    left outer join lesson as lesson2 on course.id = lesson2.course_id
where lesson.id = 4
order by main_id desc
;

select
lesson.id as id,
lesson.name as name,
lesson.course_id as course_id,
course.title,
course.start_date,
course.finish_date
from lesson left outer join course on lesson.course_id = course.id
order by lesson.course_id;

select
    course.id,
    course.title,
    course.start_date,
    course.finish_date,
    coalesce(lesson.id, -1) as lesson_id,
    lesson.name
from course
    left outer join lesson on course.id = lesson.course_id
where course.id = 4;

select
    course.id,
    course.title,
    course.start_date,
    course.finish_date,
    coalesce(lesson.id, -1) as lesson_id,
    lesson.name
from course
         left outer join lesson on course.id = lesson.course_id
order by course.id;