package ru.itdrive.analysis.app;
import ru.itdrive.analysis.utils.FileUtility;
import com.beust.jcommander.JCommander;

public class Program {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
        .addObject(arguments)
        .build()
        .parse(args);    

        FileUtility fileUtility = new FileUtility();
        fileUtility.ls("resources/Helga.gif");

        fileUtility.ls(arguments.fileName);
       
    }
}