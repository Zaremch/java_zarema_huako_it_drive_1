package ru.itdrive.analysis.utils;
import java.io.File;
import java.io.IOException;

public class FileUtility {
   
    public void ls(String currentFolder) {
        
        try {
            File file = new File(currentFolder);
            System.out.printf("%s %.2f KB", file.getName(), size(file)/1024);
            System.out.println();
            } catch(Exception ex) {
                System.out.println("\n" + ex.toString());
            }
    }

    private float size(File file) {
        if (file.isFile()) {
            return file.length();
        }
        float sum = 0;
        for (File subordinateFile : file.listFiles()) {
            sum += size(subordinateFile);
        }
        return sum;
    }
}
