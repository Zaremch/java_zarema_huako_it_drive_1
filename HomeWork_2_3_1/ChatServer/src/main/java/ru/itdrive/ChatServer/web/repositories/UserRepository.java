package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.Room;
import ru.itdrive.ChatServer.web.models.User;

public interface UserRepository extends CrudRepository<User> {
    User findByUser(User user);
    void saveToRoom(User user, Room room);
}
