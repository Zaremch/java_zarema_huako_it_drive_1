package ru.itdrive.ChatServer.web.services;

import ru.itdrive.ChatServer.web.models.Message;

import java.util.List;

public interface MessagesService {
    Integer addUser(String nickname);
    Integer addRoom(String title, Integer countUser);
    Message sendMessage(Integer idUser, Integer idRoom, String message);
    List<Message> findAllLastMessages(int limit, Integer idRoom);
    void saveToRoom(Integer idUser, Integer idRoom);
    Integer findByLastUserVisit(Integer idUser);
}
