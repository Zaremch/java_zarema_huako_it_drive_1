package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.Message;
import ru.itdrive.ChatServer.web.models.Room;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message> {
    List<Message> findAllLastMessages(int limit, Room room);
}
