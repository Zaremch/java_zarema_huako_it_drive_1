package ru.itdrive.ChatServer.web.repositories;

import java.util.List;

public interface CrudRepository<T> {
    T save(T object);

    void update(T object);

    void delete(T object);

    T find(Integer id);

    List<T> findALL();
}