package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.Message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

//Спросить
//SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
//        SLF4J: Defaulting to no-operation (NOP) logger implementation
//        SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.

@Component (value = "socketServerImpl")
public class SocketServerImpl implements SocketServer {

    private List<ChatSocketClient> anonyms;
    private Map<Integer, List<ChatSocketClient>> users;

    @Autowired
    private MessagesService messagesService;

    public SocketServerImpl() {
        this.anonyms = new ArrayList<>();
        this.users = new HashMap<>();
    }

    private class ChatSocketClient extends Thread {

        private Integer userId; // id-пользователя
        private Integer roomId; // id-текущей комнаты пользователя
        private Socket socket; // объектная переменная для взаимодействия с пользователям в рамках протокола сокетов.

        private BufferedReader input; // входной поток для чтения символов от сокета-клиента
        private PrintWriter output; // выходной поток для записи символов от сокета-сервера сокету-клиенту

        public ChatSocketClient(Socket socketClient) {
            this.socket = socketClient;
            this.userId = -1;
            this.roomId = -1;
        }

        public void run() {
            while (true) {

                try {

                    InputStream clientInputStream = socket.getInputStream();
                    this.input = new BufferedReader(new InputStreamReader(clientInputStream));

                    String[] parseCommand;
                    String inputLine;
                    while (true) {

                        inputLine = this.input.readLine();

                        System.out.println(inputLine);

                        parseCommand = parseCommand(inputLine);
                        if (parseCommand[0].equals("addUser")) {

                            this.userId = messagesService.addUser(parseCommand[1]);
                            this.roomId = messagesService.findByLastUserVisit(this.userId);
                            addUser();

                        } else if (parseCommand[0].equals("chooseRoom")) {

                            logOut();
                            this.roomId = messagesService.addRoom(parseCommand[1], 0);
                            addUser();
                            List<Message> lastMessages = messagesService.findAllLastMessages(30, this.roomId);

                            for (Message message : lastMessages) {
                                output = new PrintWriter(this.socket.getOutputStream(), true);
                                output.println(message.toString());
                            }

                            messagesService.saveToRoom(userId, roomId);

                        } else if (parseCommand[0].equals("logOut")) {

                            logOut();
                            this.roomId = -1;

                        } else if (parseCommand[0].equals("addMessage")) {

                            Message message = messagesService.sendMessage(this.userId, this.roomId, parseCommand[1]);
                            sendMessage(message.toString());
                        }
                    }

                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }

        }

        public void sendMessage(String message) {

            List<ChatSocketClient> listChatSocketClient = users.get(this.roomId);
            if (listChatSocketClient == null) {
                return;
            }

            for (ChatSocketClient chatSocketClient : listChatSocketClient) {

                if (chatSocketClient.roomId != this.roomId) {
                    continue;
                }

                if (chatSocketClient == this) {
                    continue;
                }

                try {
                    output = new PrintWriter(chatSocketClient.socket.getOutputStream(), true);
                    output.println(message);
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }

            }

        }

        private void logOut() {

            List<ChatSocketClient> listChatSocketClient = users.get(this.roomId);
            if (listChatSocketClient == null) {
                return;
            }
            listChatSocketClient.remove(this);
        }

        private void addUser() {

            if (!(this.userId != null && this.roomId != null)) {
                return;
            }

            if (users.containsKey(this.roomId)) {

                List<ChatSocketClient> listChatSocketClient = users.get(this.roomId);
                boolean itemFound = false;
                for (ChatSocketClient chatSocketClient : listChatSocketClient) {
                    if (chatSocketClient == this) {
                        itemFound = true;
                        break;
                    }
                }
                if (!itemFound) {
                    listChatSocketClient.add(this);
                }

            } else {

                List<ChatSocketClient> listChatSocketClient = new ArrayList<>();
                listChatSocketClient.add(this);
                users.put(this.roomId, listChatSocketClient);

            }

        }

        private String[] parseCommand(String textClient) {

            String[] command = new String[2];

            if (textClient.equals("")) {
                command[0] = "";
                command[1] = "";
                return command;
            }

            String valueCommand = "";
            String teamName = "";

            if (textClient.indexOf("Choose nickname") != -1) {
                valueCommand = textClient.replace("Choose nickname", "");
                valueCommand = valueCommand.replace("<", "");
                valueCommand = valueCommand.replace(">", "");
                valueCommand = valueCommand.trim();

                teamName = "addUser";

            } else if (textClient.indexOf("Choose room") != -1) {

                valueCommand = textClient.replace("Choose room", "");
                valueCommand = valueCommand.replace("<", "");
                valueCommand = valueCommand.replace(">", "");
                valueCommand = valueCommand.trim();

                teamName = "chooseRoom";

            } else if (textClient.equals("Exit from room")) {

                teamName = "logOut";

            } else {

                teamName = "addMessage";
                valueCommand = textClient;

            }

            command[0] = teamName;
            command[1] = valueCommand;

            return command;
        }

    }

    @Override
    public void start(int port) {
        try {

            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {

                Socket socketClient = serverSocket.accept();

                SocketServerImpl.ChatSocketClient ChatSocketClient = new SocketServerImpl.ChatSocketClient(socketClient);
                ChatSocketClient.start();

                this.anonyms.add(ChatSocketClient);

            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
