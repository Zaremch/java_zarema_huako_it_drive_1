package ru.itdrive.ChatServer.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Message {
    private Integer id;
    private String text;
    private Date dateOfPost;
    private Room room;
    private User user;

    @Override
    public String toString() {
        return user + ": " + text;
    }
}

