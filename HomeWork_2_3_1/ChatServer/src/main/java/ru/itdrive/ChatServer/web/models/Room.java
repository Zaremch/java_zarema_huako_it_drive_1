package ru.itdrive.ChatServer.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Room {
    private Integer id;
    private String title;
    private Integer countUser;
}
