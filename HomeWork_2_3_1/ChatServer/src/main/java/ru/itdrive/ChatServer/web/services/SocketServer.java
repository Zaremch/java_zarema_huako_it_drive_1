package ru.itdrive.ChatServer.web.services;

public interface SocketServer {
    void start(int port);
}
