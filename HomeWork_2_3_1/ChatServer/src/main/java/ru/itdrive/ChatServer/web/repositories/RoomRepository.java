package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.Room;
import ru.itdrive.ChatServer.web.models.User;

public interface RoomRepository extends CrudRepository<Room> {
    Room findByRoom(Room room);
    Room findByLastUserVisit(User user);
}
