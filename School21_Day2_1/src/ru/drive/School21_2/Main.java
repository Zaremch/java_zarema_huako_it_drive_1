package ru.drive.School21_2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class Main {

    public static final String NAME_FILE = "resources\\result.txt";

    public static void main(String[] args) {

        Signatures signatures = new Signatures();

        Scanner scanner = new Scanner(System.in);

        String fileName = "";
        String format = "";
        LinkedList<String> fileFormats = new LinkedList<>();

        while (true) {

            fileName = scanner.nextLine();
            if (fileName.equals("42")) {
                break;
            }

            format = signatures.extensionfiles(fileName);
            System.out.println(format);

            fileFormats.add(format);
        }

        writeFormat(fileFormats);

    }

    public static void writeFormat(LinkedList<String> fileFormats) {

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(NAME_FILE);

            for (String i:fileFormats) {
                fileOutputStream.write(i.getBytes());
                fileOutputStream.write(10);
            }

            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

}
