package ru.drive.School21_2;

import org.omg.CORBA.Any;
import org.omg.CORBA.Object;
import org.omg.CORBA.TypeCode;
import org.omg.CORBA.portable.InputStream;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Signatures {
    private final String NAME_FILE = "resources\\signatures.txt";
    private int[][] magicNumbers;
    private Map<Integer, String> numberFormat;
    private int count;
    private int maxCountMagicNumbers; //максимальное количество hex чисел в расширениях

    public Signatures() {
        magicNumbers = new int[100][100];
        count = 0;
        numberFormat = new HashMap<Integer, String>();
        maxCountMagicNumbers = 0;
        readSignatures();
    }

    public String extensionfiles(String nameFile) {

        try {
            FileInputStream fileInputStream = new FileInputStream(nameFile);

            int[] arrayBuffer = new int[maxCountMagicNumbers + 1];
            int current = 0;
            for (int i = 1; i <= maxCountMagicNumbers; i++) {
                current = fileInputStream.read();
                if (current == -1) {
                    break;
                }
                arrayBuffer[i] = current;
            }

            fileInputStream.close();

            boolean formatFound;
            for (int i = 0; i < count; i++) {

                formatFound = true;
                for (int j = 1; j <= magicNumbers[i][0]; j++) {

                    if (magicNumbers[i][j] != arrayBuffer[j]) {
                        formatFound = false;
                        break;
                    }
                }
                if (formatFound) {
                    return numberFormat.get(i);
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return "UNDEFINED";
    }

    private void readSignatures() {
        try {
            FileInputStream fileInputStream = new FileInputStream(NAME_FILE);
            int i;
            String currentLine = "";
            while ((i = fileInputStream.read()) != -1) {
                if (i == 10) {
                    addMagicNumbers(currentLine);
                    currentLine = "";
                } else {
                    currentLine = currentLine + (char) i;
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void addMagicNumbers(String currentLine) {

        if (currentLine.equals("")) {
            return;
        }

        char[] arrayСharacters = currentLine.toCharArray();
        String element = "";
        String format = "";
        int index = 0;

        for (int i = 0; i < arrayСharacters.length; i++) {
            if (arrayСharacters[i] == ',') {
                format = element;
                element = "";
                continue;
            }

            if (arrayСharacters[i] == ' ') {
                if (!element.equals("")) {
                    index++;
                    magicNumbers[count][index] = toIntHex(element);
                    element = "";
                }
                ;
                continue;
            }
            element = element + arrayСharacters[i];

            if (i == arrayСharacters.length && !element.equals("")) {
                index++;
                magicNumbers[count][index] = toIntHex(element);
            }
        }

        magicNumbers[count][0] = index; //запомним сколько нужно байтов для проверки данного формата

        if (index > maxCountMagicNumbers) {
            maxCountMagicNumbers = index;
        }

        numberFormat.put(count, format); //индекс массива magicNumbers как ключ к наименованию типа файла

        count = count + 1; //количество форматов в файле signatures
    }

    private int toIntHex(String hexString) {

        int decimalNumber = 0;
        char[] arrayHex = hexString.toCharArray();
        for (int i = 0; i < arrayHex.length; i++) {
            decimalNumber = decimalNumber * 16 + numeral(arrayHex[i]);
        }

        return decimalNumber;
    }

    private int numeral(char numeralHex) {

        int numeral;

        if (numeralHex == 'A') {
            numeral = 10;
        } else if (numeralHex == 'B') {
            numeral = 11;
        } else if (numeralHex == 'C') {
            numeral = 12;
        } else if (numeralHex == 'D') {
            numeral = 13;
        } else if (numeralHex == 'E') {
            numeral = 14;
        } else if (numeralHex == 'F') {
            numeral = 15;
        } else {
            numeral = numeralHex - 48;
        }
        ;

        return numeral;
    }

}
