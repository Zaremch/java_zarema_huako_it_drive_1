create table user_file
(
    id           serial not null
        constraint user_file_pkey
            primary key,
    user_id      integer
        constraint user_file_user_id_fkey
            references users,
    name         char(500),
    content_type char(100)
);

alter table user_file
    owner to postgres;


create table users
(
    id       serial not null
        constraint users_pkey
            primary key,
    nickname char(100),
    password char(100),
    token    char(100)
);

alter table users
    owner to postgres;

