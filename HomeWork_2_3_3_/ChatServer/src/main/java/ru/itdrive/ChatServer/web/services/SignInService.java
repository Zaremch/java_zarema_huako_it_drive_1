package ru.itdrive.ChatServer.web.services;

import ru.itdrive.ChatServer.web.models.Authentication;

import java.util.Optional;

public interface SignInService {
    Optional <Authentication> authenticate(String nickname, String password);
}
