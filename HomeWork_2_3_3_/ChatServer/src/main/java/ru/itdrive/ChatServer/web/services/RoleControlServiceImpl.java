package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.config.RolesConfig;

import java.util.Map;

@Component
public class RoleControlServiceImpl implements RoleControlService {

    @Autowired
    RolesConfig rolesConfig;

    @Override
    public boolean rightUseURL(String rolesUser, String urlUser) {

        if (rolesUser == null) {
            return false;
        }

        //Как-то не очень тут... Может можно иначе? Не нашла
        rolesUser = rolesUser.replace("[", "");
        rolesUser = rolesUser.replace("]", "");
        String[] rolesArray = rolesUser.split(",");

        Map<String, String[]> listRoles = rolesConfig.getListRoles();

        String[] allowedUrl = null;
        for (String role : rolesArray) {
            allowedUrl = listRoles.get(role);
            if (allowedUrl == null) {
                continue;
            }

            for (String url : allowedUrl) {
                if (urlUser.equals(url)) {
                    return true;
                }
            }
        }


        return false;
    }
}
