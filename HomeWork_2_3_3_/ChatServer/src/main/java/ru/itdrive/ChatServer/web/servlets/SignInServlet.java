package ru.itdrive.ChatServer.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.ChatServer.web.filters.ResponseUtil;
import ru.itdrive.ChatServer.web.models.Authentication;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.services.SignInService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignInService service;

    @Override
    public void init(ServletConfig config) throws ServletException {

        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        service = springContext.getBean(SignInService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/signInPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");

       Optional<Authentication> authenticationOptional = service.authenticate(nickname, password);

        if (authenticationOptional.isPresent()) {

            Authentication authentication = authenticationOptional.get();

            HttpSession session = request.getSession();
            session.setAttribute("authenticated", authentication.getAuthenticated());

            String[] roles = authentication.getUser().getRoles();

            session.setAttribute("roles", Arrays.toString(roles));

            String redirect = request.getParameter("redirect");

            if (redirect == null) {
                response.sendRedirect("/profile");
            } else {
                response.sendRedirect(redirect);
            }

        } else {

            ResponseUtil.sendForbidden(request, response);
        }
    }
}
