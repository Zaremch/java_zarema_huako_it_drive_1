package ru.itdrive.ChatServer.web.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Component
public class RolesConfig {
    private Map<String, String[]> listRoles;
}