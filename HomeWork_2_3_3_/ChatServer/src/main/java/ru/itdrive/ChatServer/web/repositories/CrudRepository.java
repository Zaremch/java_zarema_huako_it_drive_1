package ru.itdrive.ChatServer.web.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    T save(T object);

    void update(T object);

    void delete(T object);

    T find(Integer id);

    List<T> findALL();

    Optional <T> findOne(Integer id);
}