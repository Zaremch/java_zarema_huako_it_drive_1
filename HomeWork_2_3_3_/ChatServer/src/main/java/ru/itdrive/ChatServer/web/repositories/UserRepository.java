package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User> {
    User findByUser(User user);
    User findByToken(User user);
    Optional <User> findOneByNickName(String nickname);
}
