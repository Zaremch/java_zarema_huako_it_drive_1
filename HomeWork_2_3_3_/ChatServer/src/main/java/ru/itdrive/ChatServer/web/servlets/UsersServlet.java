package ru.itdrive.ChatServer.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private UsersService service;

    @Override
    public void init(ServletConfig config) throws ServletException {

        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        service = springContext.getBean(UsersService.class);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIdAsString = request.getParameter("userId");
        Integer userId = Integer.parseInt(userIdAsString);

        Optional<User> userOptional = service.getUserById(userId);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            request.setAttribute("user", user);

            request.getRequestDispatcher("jsp/usersPage.jsp").forward(request, response);
        } else {
            response.setStatus(404);
            request.getRequestDispatcher("html/errorPage.html").forward(request, response);
        }
    }
}
