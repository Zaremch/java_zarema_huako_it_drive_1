package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource(value = "classpath:application-production.properties")
@ComponentScan(basePackages = "ru.itdrive.ChatServer")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public Map gameParticipantsChar() {

        Map<String,String> gameParticipantsChar = new HashMap<>();
        gameParticipantsChar.put("enemy.char", environment.getProperty("enemy.char"));

        return gameParticipantsChar;

    }

}
