package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("--current-folder=");
        FileUtility fileUtility = new FileUtility(scanner.next());
        System.out.println(fileUtility.getCurrentFolder());

        String currentTeam = "";
        while (true) {
            currentTeam = scanner.next();
            if (currentTeam.equals("exit")) {
                break;
            } else if (currentTeam.equals("ls")) {
                fileUtility.ls();
            } else if (currentTeam.equals("cd")) {
                fileUtility.cd(scanner.next());
            } else if (currentTeam.equals("mv")) {
                fileUtility.mv(scanner.next(), scanner.next());
            }
        }

    }
}
