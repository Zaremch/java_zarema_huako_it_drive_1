package com.company;

import java.io.File;

public class FileUtility {
    private String currentFolder = "";

    public FileUtility(String currentFolder) {
        if (currentFolder == "") {
            this.currentFolder = "C:\\";
        } else {
            this.currentFolder = currentFolder;
        }
    }

    public String getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(String currentFolder) {
        this.currentFolder = currentFolder;
    }

    public void ls() {
        try {
            File folder = new File(currentFolder);
            for (File file : folder.listFiles()) {
                System.out.printf("%s %.2f КБ", file.getName(), size(file) / 1024);
                System.out.println();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void cd(String folder) {
        this.currentFolder = fullDirectory(folder);
        System.out.println(currentFolder);
    }

    public void mv(String what, String where) {
        try {
            where = fullDirectory(where);
            File fileWhere = new File(where);

            if (fileWhere.isDirectory()) {
                where = where + "\\" + what;
                fileWhere = new File(where);
            }

            what = currentFolder + "\\" + what;
            File fileWhat = new File(what);

            if (!fileWhat.renameTo(fileWhere)) {
                System.out.println("Не удалось переименовать файл");
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

    }

    private float size(File file) {
        if (file.isFile()) {
            return file.length();
        }
        float sum = 0;
        for (File subordinateFile : file.listFiles()) {
            sum += size(subordinateFile);
        }
        return sum;
    }

    private String fullDirectory(String folder) {

        char[] arrayCharFolder = folder.toCharArray();
        int i = 0;
        String fullDirectory = "";

        while (true) {
            if (arrayCharFolder[i] == '.' && arrayCharFolder[i + 1] == '.' && arrayCharFolder[i + 2] == '/') {

                if (fullDirectory.equals("")) {
                    fullDirectory = parentFolder(currentFolder);
                } else {
                    fullDirectory = parentFolder(fullDirectory);
                }

                i += 3;

            } else {
                break;
            }

        }

        //если есть ../, то уже на входе не краткий путь
        boolean isShort = fullDirectory.equals("");
        for (int j = i; j < arrayCharFolder.length; j++) {
            fullDirectory = fullDirectory + arrayCharFolder[j];
            if (arrayCharFolder[j] == '\\') {
                isShort = false;
            }
        }

        if (isShort) {
            fullDirectory = currentFolder + '\\' + fullDirectory;
        }

        return fullDirectory;
    }

    private String parentFolder(String folder) {
        String parentFolder = "";
        try {
            File file = new File(folder);
            parentFolder = file.getParent() + '\\';
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        return parentFolder;
    }

}
