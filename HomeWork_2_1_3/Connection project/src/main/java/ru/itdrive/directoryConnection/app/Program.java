package ru.itdrive.directoryConnection.app;
import ru.itdrive.directory.utils.DirectoryAnalysis;
import java.util.*;
import java.io.File;

public class Program {

    public static void main(String[] args) {
		
		DirectoryAnalysis directoryAnalysis = new DirectoryAnalysis();
		Map<String, LinkedList<File>> filesCatalogs = directoryAnalysis.filesCatalogs(args);
		
		LinkedList<File> files;
		for(Map.Entry<String, LinkedList<File>> item : filesCatalogs.entrySet()){
			
			System.out.printf("Key: %s ", item.getKey());
			
			files = item.getValue();
			for(File file:files) { 
				System.out.printf(" - %s; \n", file.getName());
			}
			
			System.out.println("___________________________________________________");

		}

    }
}