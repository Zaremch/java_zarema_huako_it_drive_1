package ru.itdrive.directory.utils;

import com.beust.jcommander.JCommander;
import java.util.*;
import java.io.File;

public class DirectoryAnalysis {
	
	public Map<String, LinkedList<File>> filesCatalogs(String[] args) {
		
		Arguments arguments = new Arguments();
        JCommander.newBuilder()
        .addObject(arguments)
        .build()
        .parse(args);    

		List<String> Catalogs = arguments.Catalogs;
		ArrayList<CatalogAnalysisThread> listThread = new ArrayList<CatalogAnalysisThread>();
		
		CatalogAnalysisThread thread;
		for (String catalog:Catalogs) {
			
			thread = new CatalogAnalysisThread(catalog);
			thread.start();
						
			listThread.add(thread);		
		}
	
		
		Map<String, LinkedList<File>> filesCatalogs = new HashMap<String, LinkedList<File>>();
		
		for (CatalogAnalysisThread сurrentThread:listThread) {
			try {
				
                сurrentThread.join();
				filesCatalogs.put(сurrentThread.getCatalog(), сurrentThread.getFiles());
				
				
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }			
		}	
		
		 return filesCatalogs;
	}
	
}	