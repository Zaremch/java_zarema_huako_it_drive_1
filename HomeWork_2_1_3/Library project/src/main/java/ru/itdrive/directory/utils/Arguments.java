package ru.itdrive.directory.utils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.ArrayList;
import java.util.List;

@Parameters(separators = "=")
public class Arguments {
	
	@Parameter(names = {"--catalog"}, description = "The Catalog")
	public List<String> Catalogs = new ArrayList<String>();

}