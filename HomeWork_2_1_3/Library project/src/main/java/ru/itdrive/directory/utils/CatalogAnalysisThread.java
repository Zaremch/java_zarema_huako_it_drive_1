package ru.itdrive.directory.utils;

import java.io.File;
import java.util.LinkedList;
import ru.itdrive.directory.utils.FileUtility;

public class CatalogAnalysisThread extends Thread{

	private String catalog;
	private LinkedList<File> files;

	public CatalogAnalysisThread(String catalog) {
		this.catalog = catalog;
		this.files = new LinkedList<File>();  
	}

	public LinkedList<File> getFiles() {
		return files;
    }
	
	public String getCatalog() {
        return catalog;
    }
	
	@Override
	public void run() {
		
		FileUtility fileUtility = new FileUtility(catalog);
		files = fileUtility.ls(); 	
		
	}	
}