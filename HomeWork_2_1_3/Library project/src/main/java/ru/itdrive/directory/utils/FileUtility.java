package ru.itdrive.directory.utils;

import java.io.File;
import java.util.LinkedList;

public class FileUtility {

    private String rootFolder;

    public FileUtility(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public String getCurrentFolder() {
        return rootFolder;
    }

    public void setCurrentFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public LinkedList<File> ls() {
	
        LinkedList<File> files = new LinkedList<File>();

        try {
            File file = new File(rootFolder);
            if (file.exists()) {
                addCatalogFiles(files, file);
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

        return files;
    }

    private void addCatalogFiles(LinkedList<File> files, File currentFile) {

        if (currentFile.isFile()) {
            files.add(currentFile);
            return;
        }

        try {

            for (File subordinateFile : currentFile.listFiles()) {
                addCatalogFiles(files, subordinateFile);
            }

        } catch (Exception e) {
            //Если папка недоступная, скрытая, то выходит ошибка. Не нашла как обработать.
            //Ну просто можно ничего не делать
            //Папка недоступна пользователю и мы ее не показываем

        }
    }

}
