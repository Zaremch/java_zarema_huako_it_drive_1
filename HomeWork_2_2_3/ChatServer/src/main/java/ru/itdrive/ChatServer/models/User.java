package ru.itdrive.ChatServer.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class User {
    private Integer id;
    private String nickname;

    @Override
    public String toString() {
        return nickname;
    }
}
