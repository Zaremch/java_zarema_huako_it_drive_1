package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryJdbcImpl implements MessageRepository {

    Connection connection;

    public MessageRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Message object) {

        try {

            //language=SQL
            String sql_text = "insert into message(text, date_of_post, room_id, author_id)\n" +
                    "values (?, ?, ?, ?);";

            PreparedStatement statement = connection.prepareStatement(sql_text);
            statement.setString(1, object.getText());
            statement.setTimestamp(2, new java.sql.Timestamp(object.getDateOfPost().getTime()));
            statement.setInt(3, object.getRoom().getId());
            statement.setInt(4, object.getUser().getId());

            statement.executeUpdate();

            statement.close();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<Message> findLastMessages(int limit, Room room) {

        //language=SQL
        String sql_text = "SELECT message.id,\n" +
                "       message.date_of_post,\n" +
                "       message.text,\n" +
                "       message.room_id,\n" +
                "       r.title,\n" +
                "       message.author_id,\n" +
                "       u.nickname\n" +
                "from message\n" +
                "         inner join users u on u.id = message.author_id\n" +
                "         inner join room r on r.id = message.room_id\n" +
                "where room_id = %room_id%\n" +
                "order by date_of_post \n" +
                "limit %limit%;";

        sql_text = sql_text.replace("%limit%", "" + limit);
        sql_text = sql_text.replace("%room_id%", "" + room.getId());

        try {

            List<Message> result = new ArrayList<Message>();

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);

            while (resultSet.next()) {

                User user = User.builder()
                        .id(resultSet.getInt("author_id"))
                        .nickname(resultSet.getString("nickname").trim())
                        .build();

                Message message = Message.builder()
                        .id(resultSet.getInt("id"))
                        .text(resultSet.getString("text").trim())
                        .dateOfPost(resultSet.getDate("date_of_post"))
                        .room(room)
                        .user(user)
                        .build();

                result.add(message);

            }

            statement.close();

            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Message find(Integer id) {
        return null;
    }

    @Override
    public List<Message> findALL() {
        return null;
    }
}
