package ru.itdrive.ChatServer.socketserver;

import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;
import ru.itdrive.ChatServer.repositories.*;

import javax.imageio.IIOException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class EchoServerSocket {

    private LinkedList<ReceiverMessageTaskClient> listTaskClient = new LinkedList();

    private class ReceiverMessageTaskClient extends Thread {

        private Socket socketClient;
        private User user;
        private Room room;
        private Connection connection;

        public ReceiverMessageTaskClient(Socket socketClient, Connection connection) {
            this.socketClient = socketClient;

            //у каждого клиента должен быть свой connection или общий на весь сервер?
            this.connection = connection;
        }

        public void run() {
            while (true) {
                try {

                    InputStream clientInputStream = socketClient.getInputStream();
                    BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));

                    String inputLine;
                    while (true) {

                        inputLine = clientReader.readLine();
                        сommandDescription(inputLine);

                    }

                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        private void сommandDescription(String textClient) {

            if (textClient.equals("")) {
                return;
            }

            String valueCommand = "";
            if (textClient.indexOf("Choose nickname") != -1) {
                //"Choose nickname <nickname>"

                valueCommand = textClient.replace("Choose nickname", "");
                valueCommand = valueCommand.replace("<", "");
                valueCommand = valueCommand.replace(">", "");
                valueCommand = valueCommand.trim();
                logIn(valueCommand);

            } else if (textClient.indexOf("Choose room") != -1) {

                valueCommand = textClient.replace("Choose room", "");
                valueCommand = valueCommand.replace("<", "");
                valueCommand = valueCommand.replace(">", "");
                valueCommand = valueCommand.trim();
                chooseRoom(valueCommand);

            } else if (textClient.equals("Exit from room")) {

                logOff();

            } else if (this.user != null && this.room != null) {
                addMessage(textClient);
            }
            ;

        }

        private void addMessage(String text) {
            MessageRepository repositoryMessage = new MessageRepositoryJdbcImpl(connection);

            Message message = Message.builder()
                    .text(text)
                    .dateOfPost(new Date())
                    .room(this.room)
                    .user(this.user)
                    .build();

            repositoryMessage.save(message);

            Integer room_id = this.room.getId();
            for (ReceiverMessageTaskClient taskClient:listTaskClient) {
                if (taskClient == this) {
                    continue;
                }

                if (taskClient.room.getId() != room_id) {
                    continue;
                }

                sendMessage(">> " + message, taskClient.socketClient);

            }

        }

        private void logOff() {

            if (this.room == null) {
                return;
            }

            UserRepository UserRepository = new UserRepositoryJdbcImpl(connection);
            UserRepository.delUserToRoom(this.user, this.room);

            this.room = null;
            sendMessage(">>", this.socketClient);

        }

        private void chooseRoom(String title) {

            if (title.equals("")) {
                return;
            }

            RoomRepository repository = new RoomRepositoryJdbcImpl(connection);
            this.room = repository.findByName(title);

            if (this.room == null) {
                this.room = repository.saveByTitle(title);
            }

            UserRepository UserRepository = new UserRepositoryJdbcImpl(connection);
            UserRepository.addUserToRoom(this.user, this.room);

            sendMessage("<<<<<<" + this.room + ">>>>>>", this.socketClient);

            MessageRepository repositoryMessage = new MessageRepositoryJdbcImpl(connection);
            List<Message> messageList = repositoryMessage.findLastMessages(30, this.room);

            for (Message message:messageList) {
                sendMessage(">> " + message, this.socketClient);
            }

        }

        private void logIn(String nickname) {

            if (nickname.equals("")) {
                nickname = "Аноним";
            }

            UserRepository repositoryUser = new UserRepositoryJdbcImpl(connection);
            this.user = repositoryUser.findByName(nickname);

            if (this.user == null) {
                this.user = repositoryUser.saveByNick(nickname);
            } else {
                RoomRepository repository = new RoomRepositoryJdbcImpl(connection);
                this.room = repository.lastVisit(this.user);
            }

            sendMessage("<<<<<<" + this.user + ">>>>>>", this.socketClient);

            if (this.room != null) {
                sendMessage("<<<<<<" + this.room + ">>>>>>", this.socketClient);
            }

        }

        public void sendMessage(String message, Socket сurrentSocket) {
            try {
                PrintWriter writer = new PrintWriter(сurrentSocket.getOutputStream(), true);
                writer.println(message);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

        }
    }

    public void start(int port, String db_url, String db_user, String db_password) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {

                Socket socketClient = serverSocket.accept();
                Connection connection = DriverManager.getConnection(db_url, db_user, db_password);

                EchoServerSocket.ReceiverMessageTaskClient receiverMessageTaskClient = new EchoServerSocket.ReceiverMessageTaskClient(socketClient, connection);
                receiverMessageTaskClient.start();

                this.listTaskClient.add(receiverMessageTaskClient);
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
