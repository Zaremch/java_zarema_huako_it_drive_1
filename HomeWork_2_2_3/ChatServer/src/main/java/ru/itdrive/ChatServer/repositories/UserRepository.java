package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

public interface UserRepository extends GrudRepository<User>{
    User saveByNick(String nickname);
    User findByName(String nickname);
    void addUserToRoom(User user, Room room);
    void delUserToRoom(User user, Room room);
}
