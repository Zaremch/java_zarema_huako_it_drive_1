package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

public interface RoomRepository extends GrudRepository<Room> {
    Room saveByTitle(String title);
    Room findByName(String title);
    Room lastVisit(User user);
}
