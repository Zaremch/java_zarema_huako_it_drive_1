package ru.itdrive.ChatServer.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.ChatServer.socketserver.EchoServerSocket;

public class MainServer {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/chat";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "QWE123qwe";

    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        EchoServerSocket serverSocket = new EchoServerSocket();
        serverSocket.start(arguments.port, DB_URL, DB_USER, DB_PASSWORD);

    }
}
