package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import java.sql.*;
import java.util.Date;
import java.util.List;

public class UserRepositoryJdbcImpl implements UserRepository {
    private Connection connection;

    public UserRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(User object) {

    }

    @Override
    public void update(User object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public User findByName(String nickname) {

        //language=SQL
        String sql_text = "SELECT * from users where nickname = '%nickname%';";
        sql_text = sql_text.replace("%nickname%", nickname);

        try {
            User user = null;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                user = User.builder()
                        .id(resultSet.getInt("id"))
                        .nickname(nickname)
                        .build();
            }

            statement.close();

            return user;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public User saveByNick(String nickname) {
        try {

            //language=SQL
            String sql_text = "INSERT into users (nickname) values (?);";
            PreparedStatement statement = connection.prepareStatement(sql_text);
            statement.setString(1, nickname);
            statement.executeUpdate();

            statement.close();

            return findByName(nickname);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void addUserToRoom(User user, Room room) {

        try {

            Integer idUser = user.getId();
            Integer idRoom = room.getId();

            //language=SQL
            String sql_text = "INSERT INTO user_rooms (room_id, user_id)\n" +
                    "SELECT * FROM (SELECT ?, ?) AS tmp\n" +
                    "WHERE NOT EXISTS (\n" +
                    "        SELECT user_id FROM user_rooms WHERE room_id = ? and user_id = ?\n" +
                    "    ) LIMIT 1;\n" +
                    "insert into room_visit (date_visit, room_id, user_id) \n" +
                    "values (?, ?, ?);";

            PreparedStatement statement = connection.prepareStatement(sql_text);
            statement.setInt(1, idRoom);
            statement.setInt(2, idUser);
            statement.setInt(3, idRoom);
            statement.setInt(4, idUser);

            java.util.Date date = new Date();
            statement.setTimestamp(5, new Timestamp(date.getTime()));

            statement.setInt(6, idRoom);
            statement.setInt(7, idUser);

            statement.executeUpdate();

            statement.close();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delUserToRoom(User user, Room room) {

        try {

            //language=SQL
            String sql_text = "delete from user_rooms where room_id = ? and user_id = ?;";

            PreparedStatement statement = connection.prepareStatement(sql_text);
            statement.setInt(1, room.getId());
            statement.setInt(2, user.getId());

            statement.executeUpdate();

            statement.close();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public User find(Integer id) {
        return null;
    }

    @Override
    public List<User> findALL() {
        return null;
    }
}
