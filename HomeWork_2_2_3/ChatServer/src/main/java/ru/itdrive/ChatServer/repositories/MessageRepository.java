package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;

import java.util.List;

public interface MessageRepository extends GrudRepository<Message>{
    List<Message> findLastMessages(int limit, Room room);
}
