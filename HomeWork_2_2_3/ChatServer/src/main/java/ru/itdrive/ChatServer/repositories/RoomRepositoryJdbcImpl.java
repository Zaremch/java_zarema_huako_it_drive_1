package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import java.sql.*;
import java.util.List;

public class RoomRepositoryJdbcImpl implements RoomRepository {
    private Connection connection;

    public RoomRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Room object) {

    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Room findByName(String title) {

        //language=SQL
        String sql_text = "Select * from room where title = '%title%'";
        sql_text = sql_text.replace("%title%", title);

        try {
            Room room = null;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                room = Room.builder()
                        .id(resultSet.getInt("id"))
                        .title(title)
                        .build();
            }

            statement.close();
            return room;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Room lastVisit(User user) {

        //language=SQL
        String sql_text = "select\n" +
                "r.room_id as id,\n" +
                "       r2.title as title\n" +
                "from room_visit as r\n" +
                "inner join room r2 on r2.id = r.room_id\n" +
                "where r.user_id = %user_id% and date_visit =(\n" +
                "SELECT max(date_visit) from room_visit where user_id = r.user_id)";

        sql_text = sql_text.replace("%user_id%", "" + user.getId());

        try {

            Room room = null;
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {

                room = Room.builder()
                        .id(resultSet.getInt("id"))
                        .title(resultSet.getString("title").trim())
                        .build();

            }

            statement.close();
            return room;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Room saveByTitle(String title) {

        try {
            //language=SQL
            String sql_text = "INSERT into room (title) values (?);";
            PreparedStatement statement = connection.prepareStatement(sql_text);
            statement.setString(1, title);
            statement.executeUpdate();

            statement.close();

            return findByName(title);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Room find(Integer id) {
        return null;
    }

    @Override
    public List<Room> findALL() {
        return null;
    }
}
