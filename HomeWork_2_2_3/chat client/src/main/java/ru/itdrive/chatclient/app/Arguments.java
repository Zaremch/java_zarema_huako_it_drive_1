package ru.itdrive.chatclient.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(
        separators = "="
)
public class Arguments {
    @Parameter(
            names = {"--serverPort"}
    )
    public Integer serverPort;

    @Parameter(
            names = {"--serverHost"}
    )
    public String serverHost;

}