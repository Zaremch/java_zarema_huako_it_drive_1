package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User> {
    Optional<User> findByUser(User user);
    Optional<User> findByToken(User user);
}
