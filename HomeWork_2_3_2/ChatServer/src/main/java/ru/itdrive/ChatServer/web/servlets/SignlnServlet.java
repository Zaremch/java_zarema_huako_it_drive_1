package ru.itdrive.ChatServer.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.ChatServer.web.config.ApplicationConfig;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.services.AuthorizationService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@WebServlet("/signIn")
public class SignlnServlet extends HttpServlet {

    private AuthorizationService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(AuthorizationService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("html/signln.html");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");

        Optional<User> userOptional = service.findByUser(nickname);

        if (!userOptional.isPresent()) {
            return;
        }

        User user = userOptional.get();
        if (!password.equals(user.getPassword())) {
            return;
        }

        String newToken = UUID.randomUUID().toString();

        user.setToken(newToken);
        service.updateUser(user);

        Cookie cookie = new Cookie("Token", newToken);
        response.addCookie(cookie);

    }
}
