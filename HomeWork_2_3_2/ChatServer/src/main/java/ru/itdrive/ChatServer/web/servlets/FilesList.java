package ru.itdrive.ChatServer.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.ChatServer.web.config.ApplicationConfig;
import ru.itdrive.ChatServer.web.models.FileUser;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.services.FilesService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/fileList")
public class FilesList extends HttpServlet {

    FilesService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        String token = "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("Token")) {
                token = cookie.getValue();
                break;
            }
        }

        if (token.equals("")) {
            return;
        }

        List<FileUser> fileUserList = service.findAllByUser(token);

        String textHtml = "<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\"\n" +
                "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                "    <title>Document</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h2>%Document%</h2>\n" +
                "%filesList%\n" +
                "</body>\n" +
                "</html>";

        String filesList = "";
        String userName = "";
        User user = null;
        String name = "";
        for (FileUser fileUser : fileUserList) {

            name = fileUser.getName();

            filesList += "<p><a href=\"/file?id=" + fileUser.getUuid()
                    + "&name=" + name
                    + "&contentType=" + fileUser.getContentType()
                    + "\" >" + name + "</a></p>\n";

            user = fileUser.getUser();
            if (user != null) {
                userName = user.getNickname();
            }

        }

        textHtml = textHtml.replace("%filesList%", filesList);
        textHtml = textHtml.replace("%Document%", userName);

        PrintWriter writer = response.getWriter();
        writer.println(textHtml);
    }
}
