package ru.itdrive.ChatServer.web.repositories;

import ru.itdrive.ChatServer.web.models.FileUser;
import ru.itdrive.ChatServer.web.models.User;

import java.util.List;

public interface FileUserRepository extends CrudRepository<FileUser>{
    List<FileUser> findAllByUser(User user);
}
