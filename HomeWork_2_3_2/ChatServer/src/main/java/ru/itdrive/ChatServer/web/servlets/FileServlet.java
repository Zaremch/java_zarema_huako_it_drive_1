package ru.itdrive.ChatServer.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/file")
public class FileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        String contentType = request.getParameter("contentType");
        String name = request.getParameter("name");

        File file = new File("C://files/" + id + "_" + name);
        response.setContentType(contentType);
        response.setContentLength((int) file.length());

        response.setHeader("Content-Disposition", "filename=\"" + name + "\"");

        Files.copy(file.toPath(), response.getOutputStream());
        response.flushBuffer();

    }
}
