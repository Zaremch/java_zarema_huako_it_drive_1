package ru.itdrive.ChatServer.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FileUser {
    private Integer id;
    private User user;
    private String name;
    private String contentType;
    private String uuid;
}
