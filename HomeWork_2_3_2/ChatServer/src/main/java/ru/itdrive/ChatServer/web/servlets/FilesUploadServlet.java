package ru.itdrive.ChatServer.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.ChatServer.web.config.ApplicationConfig;
import ru.itdrive.ChatServer.web.models.FileUser;
import ru.itdrive.ChatServer.web.services.FilesService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@WebServlet("/files")
@MultipartConfig()
public class FilesUploadServlet extends HttpServlet {

    FilesService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/fileUpload.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        String token = "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("Token")) {
                token = cookie.getValue();
                break;
            }
        }

        if (token.equals("")) {
            return;
        }

        Part part = request.getPart("uploadedFile");
        if (part == null) {
            return;
        }

        String fileName = part.getSubmittedFileName();
        String сontentType = part.getContentType();

        FileUser fileUser = service.addFileUser(token, fileName, сontentType);

        String newName = service.catalogFiles() + fileUser.getUuid() + "_" + fileName;
        Files.copy(part.getInputStream(), Paths.get(newName));

        response.sendRedirect("/files");

    }
}
