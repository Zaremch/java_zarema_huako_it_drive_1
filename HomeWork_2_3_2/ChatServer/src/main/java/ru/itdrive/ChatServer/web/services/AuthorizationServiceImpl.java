package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.repositories.UserRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Component
public class AuthorizationServiceImpl implements AuthorizationService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> findByUser(String nickname) {

        User user = User.builder()
                .nickname(nickname)
                .id(-1)
                .build();

        return userRepository.findByUser(user);
    }

    @Override
    public void updateUser(User user) {
        userRepository.update(user);
    }
}
