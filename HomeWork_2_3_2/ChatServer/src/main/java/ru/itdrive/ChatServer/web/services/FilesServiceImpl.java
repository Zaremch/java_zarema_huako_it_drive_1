package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.FileUser;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.repositories.FileUserRepository;
import ru.itdrive.ChatServer.web.repositories.UserRepository;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class FilesServiceImpl implements FilesService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileUserRepository fileUserRepository;

    @Autowired
    private String catalogFiles;

    @Override
    public FileUser addFileUser(String token, String fileName, String contentType) {

        User user = User.builder()
                .token(token)
                .id(-1)
                .build();

        Optional <User> userOptional = userRepository.findByToken(user);

        if (!userOptional.isPresent()) {
            return null;
        }

        user = userOptional.get();

        UUID uuid = UUID.randomUUID();

        FileUser fileUser = FileUser.builder()
                .name(fileName)
                .contentType(contentType)
                .user(user)
                .id(-1)
                .uuid(uuid.toString())
                .build();

        fileUser = fileUserRepository.save(fileUser);
        return fileUser;
    }

    @Override
    public List<FileUser> findAllByUser(String token) {

        User user = User.builder()
                .token(token)
                .id(-1)
                .build();

        Optional<User> userOptional = userRepository.findByToken(user);

        if (!userOptional.isPresent()) {
            return null;
        }

        user = userOptional.get();

        List<FileUser> fileUserList = fileUserRepository.findAllByUser(user);
        return fileUserList;
    }

    @Override
    public String catalogFiles() {
        return this.catalogFiles;
    }

}
