package ru.itdrive.ChatServer.web.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryJdbcImpl implements UserRepository {

    private DataSource dataSource;

    public UserRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public User save(User user) {

        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            //language=SQL
            String sql_text = "INSERT into users (nickname) values (?);";

            Connection connection = this.dataSource.getConnection();
            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getNickname());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("User save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Integer id = generatedKeys.getInt("id");
                user.setId(id);
            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public void update(User user) {

        PreparedStatement statement = null;
        Connection connection = null;

        try {

            //language=SQL
            String sql_text = "UPDATE users\n" +
                    "SET nickname = ?,\n" +
                    "    password = ?,\n" +
                    "    token    = ?\n" +
                    "WHERE id = ?";

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(sql_text);
            statement.setString(1, user.getNickname());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getToken());
            statement.setInt(4, user.getId());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("User save not executed");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }

        }

    }

    @Override
    public void delete(User object) {

    }

    @Override
    public Optional<User> findByUser(User user) {

        //language=SQL
        String sql_text = "SELECT * from users where nickname = '%nickname%';";
        sql_text = sql_text.replace("%nickname%", user.getNickname());

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password").trim());
                return Optional.of(user);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return Optional.empty();
    }

    @Override
    public Optional<User> findByToken(User user) {

        //language=SQL
        String sql_text = "SELECT * from users where token = '%token%';";
        sql_text = sql_text.replace("%token%", user.getToken());

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password").trim());
                user.setNickname(resultSet.getString("nickname").trim());

                return Optional.of(user);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return Optional.empty();
    }

    @Override
    public User find(Integer id) {

        //language=SQL
        String sql_text = "SELECT * from users where id = '%id%';";
        sql_text = sql_text.replace("%id%", "" + id);

        User user = null;

        Statement statement = null;
        ResultSet resultSet = null;

        try {
            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                user = User.builder()
                        .id(resultSet.getInt("id"))
                        .nickname(resultSet.getString("nickname").trim())
                        .build();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public List<User> findALL() {
        return null;
    }
}
