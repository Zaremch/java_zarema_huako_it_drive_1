package ru.itdrive.ChatServer.web.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.FileUser;
import ru.itdrive.ChatServer.web.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileUserRepositoryImpl implements FileUserRepository {

    private DataSource dataSource;

    public FileUserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public FileUser save(FileUser fileUser) {

        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;

        try {

            //language=SQL
            String sql_text = "INSERT into user_file (user_id, name, content_type, uuid) values (?,?,?,?);";

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, fileUser.getUser().getId());
            statement.setString(2, fileUser.getName());
            statement.setString(3, fileUser.getContentType());
            statement.setString(4, fileUser.getUuid());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("File save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Integer id = generatedKeys.getInt("id");
                fileUser.setId(id);
            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return fileUser;
    }

    @Override
    public void update(FileUser object) {

    }

    @Override
    public void delete(FileUser object) {

    }

    @Override
    public FileUser find(Integer id) {
        return null;
    }

    @Override
    public List<FileUser> findALL() {
        return null;
    }

    @Override
    public List<FileUser> findAllByUser(User user) {

        //language=SQL
        String sql_text = "select f.id,\n" +
                "       f.name,\n" +
                "       f.content_type as contentType,\n" +
                "       f.uuid as uuid\n" +
                "from users\n" +
                "         inner join user_file as f on users.id = f.user_id\n" +
                "where token = ?";

        PreparedStatement statement = null;
        List<FileUser> result = new ArrayList<FileUser>();
        ResultSet resultSet = null;
        Connection connection = null;

        try {

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(sql_text);
            statement.setString(1, user.getToken());

            resultSet = statement.executeQuery();

            while (resultSet.next()) {

                FileUser fileUser = FileUser.builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name").trim())
                        .contentType(resultSet.getString("contentType").trim())
                        .user(user)
                        .uuid(resultSet.getString("uuid").trim())
                        .build();

                result.add(fileUser);

            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }

        }

        return result;

    }
}
