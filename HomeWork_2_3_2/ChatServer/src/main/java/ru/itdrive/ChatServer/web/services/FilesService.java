package ru.itdrive.ChatServer.web.services;

import ru.itdrive.ChatServer.web.models.FileUser;

import java.util.List;

public interface FilesService {
    FileUser addFileUser(String token, String fileName, String contentType);
    List<FileUser> findAllByUser(String token);
    String catalogFiles();
}
