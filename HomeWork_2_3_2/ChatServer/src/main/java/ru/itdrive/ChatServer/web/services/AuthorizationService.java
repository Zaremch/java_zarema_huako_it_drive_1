package ru.itdrive.ChatServer.web.services;

import ru.itdrive.ChatServer.web.models.User;
import java.util.Optional;

public interface AuthorizationService {
    Optional<User> findByUser(String nickname);
    void updateUser(User user);
}
