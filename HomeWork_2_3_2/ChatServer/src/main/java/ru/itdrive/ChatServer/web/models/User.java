package ru.itdrive.ChatServer.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class User {
    private Integer id;
    private String nickname;
    private String password;
    private String token;

    @Override
    public String toString() {
        return nickname;
    }

}
