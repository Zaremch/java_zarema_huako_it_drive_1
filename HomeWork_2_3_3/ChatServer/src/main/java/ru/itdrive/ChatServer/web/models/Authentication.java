package ru.itdrive.ChatServer.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Authentication {
    private User user;
    private Boolean authenticated;
}
