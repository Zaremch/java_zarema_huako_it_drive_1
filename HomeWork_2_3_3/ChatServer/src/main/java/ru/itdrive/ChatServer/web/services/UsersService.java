package ru.itdrive.ChatServer.web.services;

import ru.itdrive.ChatServer.web.models.User;

import java.util.Optional;

public interface UsersService {
    Optional<User> getUserById(Integer id);
}
