package ru.itdrive.ChatServer.web.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(
        separators = "="
)
public class Arguments {
    @Parameter(
            names = {"--port"}
    )
    public Integer port;


    public Arguments() {
    }
}