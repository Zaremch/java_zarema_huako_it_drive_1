package ru.itdrive.ChatServer.web.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryJdbcImpl implements UserRepository {

    private DataSource dataSource;

    //language=SQL
    private final String SQL_FIND_USER_BY_ID = "select * from users where id = ?";

    //language=SQL
    private final String SQL_FIND_USER_BY_NICKNAME = "select * from users where nickname = ?";

    public UserRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public User save(User user) {

        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            //language=SQL
            String sql_text = "INSERT into users (nickname) values (?);";

            Connection connection = this.dataSource.getConnection();
            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getNickname());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("User save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Integer id = generatedKeys.getInt("id");
                user.setId(id);
            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public void update(User user) {

        PreparedStatement statement = null;
        Connection connection = null;

        try {

            //language=SQL
            String sql_text = "UPDATE users\n" +
                    "SET nickname = ?,\n" +
                    "    password = ?,\n" +
                    "    token    = ?\n" +
                    "WHERE id = ?";

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(sql_text);
            statement.setString(1, user.getNickname());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getToken());
            statement.setInt(4, user.getId());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("User save not executed");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }

        }

    }

    @Override
    public void delete(User object) {

    }

    @Override
    public User findByUser(User user) {

        //language=SQL
        String sql_text = "SELECT * from users where nickname = '%nickname%';";
        sql_text = sql_text.replace("%nickname%", user.getNickname());

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password").trim());
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public User findByToken(User user) {

        //language=SQL
        String sql_text = "SELECT * from users where token = '%token%';";
        sql_text = sql_text.replace("%token%", user.getToken());

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password").trim());
                user.setNickname(resultSet.getString("nickname").trim());
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public Optional<User> findOneByNickName(String nickname) {

        PreparedStatement statement = null;
        ResultSet result = null;
        Connection connection = null;

        try {

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(SQL_FIND_USER_BY_NICKNAME);
            statement.setString(1, nickname);

            result = statement.executeQuery();
            if (result.next()) {

                String role = result.getString("role");
                String[] rolesArray = null;
                if (role != null) {
                    rolesArray = role.trim().split(";");
                }

                User user = User.builder()
                        .id(result.getInt("id"))
                        .token(result.getString("token"))
                        .nickname(result.getString("nickname").trim())
                        .password(result.getString("password").trim())
                        .roles(rolesArray)
                        .build();
                return Optional.of(user);
            }
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (result != null) {
                try {
                    result.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }
    }

    @Override
    public Optional<User> findOne(Integer id) {

        PreparedStatement statement = null;
        ResultSet result = null;
        Connection connection = null;

        try {

            connection = this.dataSource.getConnection();

            statement = connection.prepareStatement(SQL_FIND_USER_BY_ID);
            statement.setInt(1, id);

            result = statement.executeQuery();
            if (result.next()) {
                User user = User.builder()
                        .id(result.getInt("id"))
                        .token(result.getString("token"))
                        .nickname(result.getString("nickname"))
                        .build();
                return Optional.of(user);
            }
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (result != null) {
                try {
                    result.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {

                }
            }
        }

    }

    @Override
    public User find(Integer id) {

        //language=SQL
        String sql_text = "SELECT * from users where id = '%id%';";
        sql_text = sql_text.replace("%id%", "" + id);

        User user = null;

        Statement statement = null;
        ResultSet resultSet = null;

        try {
            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);
            if (resultSet.next()) {
                user = User.builder()
                        .id(resultSet.getInt("id"))
                        .nickname(resultSet.getString("nickname").trim())
                        .build();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return user;
    }

    @Override
    public List<User> findALL() {
        return null;
    }
}
