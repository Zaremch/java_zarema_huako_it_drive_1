package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.repositories.UserRepository;

import java.util.Optional;

@Component
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> getUserById(Integer id) {
        return userRepository.findOne(id);
    }
}
