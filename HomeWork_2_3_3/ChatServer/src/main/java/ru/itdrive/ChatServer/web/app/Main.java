package ru.itdrive.ChatServer.web.app;

import com.beust.jcommander.JCommander;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.ChatServer.web.config.ApplicationConfig;
import ru.itdrive.ChatServer.web.services.SocketServer;

public class Main {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        //ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SocketServer service = context.getBean(SocketServer.class);
        //service.start(arguments.port);

        service.start(7777);

    }
}
