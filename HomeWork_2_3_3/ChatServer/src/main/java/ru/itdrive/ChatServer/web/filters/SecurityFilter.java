package ru.itdrive.ChatServer.web.filters;

import org.springframework.context.ApplicationContext;
import ru.itdrive.ChatServer.web.services.RoleControlService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {

    private RoleControlService service;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        System.out.println("In Security Filter" + request.getRequestURI() + "?" + request.getQueryString());

        if (!isProtected(request.getRequestURI())) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        //Точно была создана. Не создавать новый
        HttpSession session = request.getSession(false);
        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");

            if (authenticated != null && authenticated) {

                String roles = (String) session.getAttribute("roles");
                if (service.rightUseURL(roles, request.getRequestURI())) {
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }

        }
        ResponseUtil.sendForbidden(request, response);
    }

    private boolean isProtected(String path) {
        return !path.startsWith("/signIn") && !path.startsWith("/favicon.ico");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        service = springContext.getBean(RoleControlService.class);
    }

    @Override
    public void destroy() {

    }
}
