package ru.itdrive.ChatServer.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.web.models.Authentication;
import ru.itdrive.ChatServer.web.models.User;
import ru.itdrive.ChatServer.web.repositories.UserRepository;

import java.util.Optional;

@Component
public class SignInServiceImpl implements SignInService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<Authentication> authenticate(String nickname, String password) {

        Optional<User> userOptional = userRepository.findOneByNickName(nickname);
        if (userOptional.isPresent()) {
            User user = userOptional.get();

            Authentication authentication = Authentication.builder()
                    .user(user)
                    .authenticated(user.getPassword().equals(password))
                    .build();

            return Optional.of(authentication);
        }

        return Optional.empty();
    }
}
