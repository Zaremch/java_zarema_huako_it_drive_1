package ru.itdrive.ChatServer.web.config;

import com.sun.javafx.collections.MappingChange;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource(value = "classpath:application.properties")
@ComponentScan(basePackages = "ru.itdrive.ChatServer.web")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public HikariConfig hikariConfig() {

        HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl(environment.getProperty("db.url"));
        hikariConfig.setPassword(environment.getProperty("db.password"));
        hikariConfig.setUsername(environment.getProperty("db.user"));
        hikariConfig.setDriverClassName(environment.getProperty("db.driver.name"));

        return hikariConfig;
    }

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    public RolesConfig rolesConfig() {

        String admin = environment.getProperty("ADMIN");
        String user = environment.getProperty("USER");

        Map<String, String[]> mapRoles = new HashMap<String, String[]>();
        mapRoles.put("ADMIN", admin.split(";"));
        mapRoles.put("USER" , user.split(";"));

        RolesConfig roles = RolesConfig.builder()
                .listRoles(mapRoles)
                .build();

        return roles;
    }
}
