package ru.itdrive.ChatServer.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseUtil {
    public static void sendForbidden(HttpServletRequest request, HttpServletResponse response) throws IOException {

        StringBuilder path = new StringBuilder();
        path.append("/signIn");

        if ((!request.getRequestURI().equals("/signIn")) && (!request.getRequestURI().equals("/"))) {
            path.append("?redirect=").append(request.getRequestURI());
        }

        String queryString = request.getQueryString();

        if (queryString != null) {
            path.append("?").append(queryString);
        }

        response.setStatus(403);
        response.sendRedirect(path.toString());
    }
}
