import java.util.Scanner;
class homework {
	public static void main(String[] eargs) {

		Scanner Scanner = new Scanner(System.in);
		int a = Scanner.nextInt();
		int b = Scanner.nextInt();

		System.out.println(sumDigits(a));
		System.out.println(mirrorView(a));
		System.out.println(sumNumbers(a, b));	
	}

	public static int sumNumbers(int number1, int number2) {

		int sumNumbers = 0;	
		for (int i = number1; i<=number2; i++) {
			sumNumbers+=i;
		}

		return sumNumbers;
	}

	public static String mirrorView(int number) {

		int numberTemp = Math.abs(number);

		String mirrorView = "";
		while (numberTemp > 10) {
			mirrorView = mirrorView + numberTemp%10;
			numberTemp = numberTemp/10;
		}
		mirrorView = mirrorView + numberTemp;
		
		if (number < 0) {
			mirrorView = "-" + mirrorView;
		}

		return mirrorView;
	}

	public static int sumDigits(int number) {

		number = Math.abs(number);
		
		int sum = 0;
		while (number > 10) {

			sum += number%10;
			number = number/10;

		}

		sum += number;
		return sum;
	}

}