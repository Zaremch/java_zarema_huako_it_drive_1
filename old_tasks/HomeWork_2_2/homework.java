import java.util.Scanner;
class homework {
	public static void main(String[] eargs) {

		Scanner Scanner = new Scanner(System.in);
		int n = Scanner.nextInt();

		double a = Scanner.nextDouble();
		double b = Scanner.nextDouble();

		System.out.println("Otvet: " + integral(a,b,n));
	}

	public static double integral(double a, double b, int n) {

		if (a==b) {
			return 0;
		}
		else if (a > b) {
			a = a + b;
			b = a - b;
			a = a - b;
		}
		else if (!isEven(n)) {
			System.out.println("Vvedeno nechetnoe cnislo n: " + n);
			return -1;
		}

		double h = (b-a)/n;
		System.out.println("h: " + h);	

		double integral = 0;
		int i = 0;

		for (double x = a; x <= b; x+=h) {	
			
			if (i==0 || i==n) {
				integral += f(x); 
			}
			else if (isEven(i)) {
				integral += 2*f(x);
			}
			else {
				integral += 4*f(x);
			}

			i++;
		}
		integral = integral*h/3;

		return integral;
	}

	public static double f(double x) {
		
		double f = 1 + 2*x*x - x*x*x;
		if (f<0) {
			System.out.println("Pri x = " + x + " ne suchestvuet");
		} 

		f = Math.sqrt(f);
		System.out.println("x: " + x + " f: " + f);
		return f;
	}

	public static boolean isEven(int Number) {

		if (Number%2==0) {
			return true;			
		}

		return false;
	}
}
