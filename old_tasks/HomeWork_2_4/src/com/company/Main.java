package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner Scanner = new Scanner(System.in);
        System.out.println("Введите n:");
        int n = Scanner.nextInt();

        System.out.println(fib(0, 1, n));
    }

    public static int fib(int a, int b, int count) {

        if (count == 0) {
            return b;
        }
        return fib(b, a + b, count - 1);
    }
}
