package com.company;

public class Main {

    public static void main(String[] args) {
	    Passenger passenger1 = new Passenger("Зарема".toCharArray());
        Passenger passenger2 = new Passenger("Женя".toCharArray());
        Passenger passenger3 = new Passenger("Красота".toCharArray());

        Bus bus = new Bus("фффф".toCharArray());

        bus.addPassengers(passenger1);
        bus.addPassengers(passenger2);
        bus.addPassengers(passenger3);

        System.out.println(bus.getPassengers()[2].getName());

    }
}
