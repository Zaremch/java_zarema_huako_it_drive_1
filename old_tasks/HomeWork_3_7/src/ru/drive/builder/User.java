package ru.drive.builder;

public class User {
    private String firstName;
    private String lastName;
    private String address;
    private int age;

    public class Builder {
        private User user;

        public Builder() {
            user = new User();
        }

        void age(int age) {
            user.age = age;
        }

        void address(String address) {
            user.address = address;
        }

        void lastName(String lastName) {
            user.lastName = lastName;
        }

        void firstName(String firstName) {
            user.firstName = firstName;
        }

        void build() {
            firstName = user.firstName;
            lastName = user.lastName;
            address = user.address;
            age = user.age;
        }
    }

    public String getFirstName() {
        return firstName;
    }
}
