package ru.drive.builder;

public class Main {

    public static void main(String[] args) {

        //Марсель, а почему нельзя разрулить просто сеторами?))
        //User user = new User();
        //user.setFirstName("Вася");
        //user.setlastName("Пупкин");
        //и т.д.

        User user = new User();
        User.Builder builder = user.new Builder();
        builder.age(20);
        builder.firstName("Вася");
        builder.lastName("Пупкин");
        builder.build();

        System.out.println(user.getFirstName());

    }
}
