import java.util.Scanner;
import java.util.Random;

class homework {
	public static void main(String[] eargs) {
		
		Scanner Scanner = new Scanner(System.in);

		System.out.println("1----------------------------");

		//Реализовать процедуру сортировки выбором.
		int array[] = newArray(10);
		printArray(array);
		array = sortArray(array);
		printArray(array);
		
		System.out.println("2----------------------------");

		//Реализовать функцию бинарного поиска через цикл.
		array = newArray(10);
		printArray(array);
		System.out.println("Vvedite chislo dlia binarnogo poiska");
		int searchNumber = Scanner.nextInt(); 
		System.out.println(searchBin(array, searchNumber));
		
		System.out.println("3----------------------------");

		double a = Scanner.nextDouble();
		double b = Scanner.nextDouble();

		int ns[] = {100, 1000, 10000, 100000, 1000000}; // разбиения
		int ys[] = {2, 3, 4, 5, 6, 7}; // степени
		
		for (int i=0; i<ns.length; i++) {
			for (int j=0; j<ys.length; j++) {
				
				System.out.println("Simpson   - " + ns[i] + " - x^" + ys[j] + " = " + integralSimpson(a, b, ns[i], ys[j]));
				System.out.println("Rectangle - " + ns[i] + " - x^" + ys[j] + " = " + integralRectangle(a, b, ns[i], ys[j]));
	
			}
		}
		
	}

	public static int searchBin(int[] array, int searchNumber) {

		int[] arraySort = sortArray(array);
		printArray(arraySort);

		int right = arraySort.length-1;
		int left  = 0;
		int mid; 
		int itemIndex = -1;
		
		while (left<=right) {

			mid = (right + left)/2;
	
			if (arraySort[mid] == searchNumber) {
				itemIndex = mid;
				break;
			} 
			else if (arraySort[mid]>searchNumber) {
				right = mid - 1;
			}
			else if (arraySort[mid]<searchNumber) {
				left = mid + 1;
			}

		}

		return itemIndex;
	}

	public static void printArray(int[] array) {
		
		for (int i = 0; i<array.length; i++) {
			System.out.print(array[i] + " ");
		}

		System.out.println("");
	}

	public static int[] sortArray(int[] array) {
		
		int[] arraySort = array;

		int iMin;
		int temp;
		for (int i = 0; i<arraySort.length-1; i++) {
			
			iMin = i;
			for (int j = i+1; j<arraySort.length; j++) {

				if (arraySort[j] < arraySort[iMin]) {
					iMin = j;
				}
			}

			if (i!=iMin) {
				temp = arraySort[i];
				arraySort[i] = arraySort[iMin];
				arraySort[iMin] = temp;
			}
			
		}

		return arraySort;

	}

	public static int[] newArray(int length) {

		int array[] = new int[length];
		Random random = new Random();
		for (int i = 0; i<length; i++) {
			array[i] = random.nextInt(100);
		}

		return array;
	}

	public static double integralRectangle(double a, double b, int n, int y) {

		if (a==b) {
			return 0;
		}
		else if (a > b) {
			a = a + b;
			b = a - b;
			a = a - b;
		}

		double h = (b-a)/n;
		double integral = 0;

		for (double x = a; x <= b; x+=h) {
			integral += f(x, y);
		}	

		integral = h*integral;
		return integral;

	} 	

	public static double integralSimpson(double a, double b, int n, int y) {

		if (!isEven(n)) {
			System.out.println("Vvedeno nechetnoe cnislo n: " + n);
			return -1;
		}

		if (a==b) {
			return 0;
		}
		else if (a > b) {
			a = a + b;
			b = a - b;
			a = a - b;
		};
		
		double h = (b-a)/n;	

		double integral = 0;
		int i = 0;

		for (double x = a; x <= b; x+=h) {	
			
			if (i==0 || i==n) {
				integral += f(x, y); 
			}
			else if (isEven(i)) {
				integral += 2*f(x, y);
			}
			else {
				integral += 4*f(x, y);
			}

			i++;
		}
		integral = integral*h/3;

		return integral;
	}

	public static double f(double x, int y) {

		double f = Math.pow(x, y);
		return f;
	
	}

	public static boolean isEven(int n) {

		if (n%2==0) {
			return true;			
		}

		return false;
	}
}
