package com.company;

import java.util.Scanner;

public class Main {

    public static final int COUNT_CHAR = 1114112;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        text = scanner.nextLine();

        //0 - количество
        //1 - количество масштабированное
        //2 - код символа
        int[][] arrayCountChar = new int[COUNT_CHAR][3];

        char[] arrayCharText = text.toCharArray();
        char currentСharacter;
        int count = 0; //Количество разных символов
        int minChar = COUNT_CHAR;
        int maxChar = 0;
        for (int i = 0; i < arrayCharText.length; i++) {
            currentСharacter = arrayCharText[i];


            if (arrayCountChar[currentСharacter][0] == 0) {
                count++;
                arrayCountChar[currentСharacter][2] = currentСharacter;
            }
            arrayCountChar[currentСharacter][0]++;

            //Чтобы потом не обходить все все символы. Как-то ограничим беспредел
            if (currentСharacter > maxChar) {
                maxChar = currentСharacter;
            }
            if (currentСharacter < minChar) {
                minChar = currentСharacter;
            }
        }

        if (count == 0) {
            return;
        }

        int indexMax = 0;
        int length = Math.min(count, 10);
        int[] temp = new int[3];
        for (int i = minChar; i < minChar + length; i++) {
            indexMax = i;
            for (int j = i + 1; j <= maxChar; j++) {
                if (arrayCountChar[j][0] > arrayCountChar[indexMax][0]) {
                    indexMax = j;
                }
            }

            if (indexMax != i) {
                temp = arrayCountChar[i];
                arrayCountChar[i] = arrayCountChar[indexMax];
                arrayCountChar[indexMax] = temp;
            }

            arrayCountChar[i][1] = arrayCountChar[i][0] * 10 / arrayCountChar[minChar][0];
        }

        for (int i = 0; i <= 11; i++) {
            for (int j = minChar; j < minChar + length; j++) {
                if (i == 11) {
                    System.out.printf("%-5s", (char) arrayCountChar[j][2]);
                } else if ((10 - i) == arrayCountChar[j][1]) {
                    System.out.printf("%-5d", arrayCountChar[j][0]);
                } else if ((10 - i) < arrayCountChar[j][1]) {
                    System.out.printf("%-5s", "#");
                }
            }//элементы
            System.out.println();
        }//высота

    }

}
