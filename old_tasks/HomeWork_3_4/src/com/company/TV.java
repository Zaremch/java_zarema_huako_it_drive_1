package com.company;

public class TV {
    private char model[];
    private Channel channels[]; //в массиве каналы по их номеру.
    private final int MAX_COUNT_CHANNELS = 10;
    RemoteController remoteController;
    private Channel included; //канал, который включен

    TV(char model[]) {
        this.model = model;
        channels = new Channel[MAX_COUNT_CHANNELS];
    }

    void setIncluded(int channelNumber) {

        if (channelNumber >= 0 & channelNumber < MAX_COUNT_CHANNELS) {
            included = channels[channelNumber];
        }
    }

    void setRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    Channel getIncluded() {
        return included;
    }

    void addChannel(Channel channel) {

        int number = channel.getNumber();
        if (number < MAX_COUNT_CHANNELS) {
            channels[number] = channel;
        }

        //у телевизора есть каналы, но у каналов нет телевизора

    }

}
