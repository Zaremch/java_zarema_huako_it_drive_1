package com.company;

public class Channel {

    private char name[];
    private int number;
    private Program programs[];
    private final int MAX_COUNT_PROGRAMS = 5;
    private int count;
    private Program currentProgram;

    Channel(char name[], int number) {
        this.name = name;

        if (number < 0) {
            number = 0;
        } else {
            this.number = number;
        }

        programs = new Program[MAX_COUNT_PROGRAMS];
    }

    Program getCurrentProgram() {
        return currentProgram;
    }

    void setСurrentProgram(int number) {
        if (number < MAX_COUNT_PROGRAMS) {
            currentProgram = programs[number]; //Текущая передача
        }

    }

    int getNumber() {
        return number;
    }

    void addProgram(Program program) {
        if (count < MAX_COUNT_PROGRAMS) {
            programs[count] = program;
            count++;
            program.setChannel(this);
        }
    }

}
