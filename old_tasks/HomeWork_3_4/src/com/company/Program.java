package com.company;

public class Program {
    private char name[];
    private Channel channel;

    Program(char name[]) {
        this.name = name;
    }

    void setChannel(Channel channel) {
        this.channel = channel;
    }

    char[] getName() {
        return name;
    }
}
