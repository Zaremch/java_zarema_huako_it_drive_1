package com.company;


import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        Program program1 = new Program("Пусть говорят".toCharArray());
        Program program2 = new Program("Поле чудес".toCharArray());
        Program program3 = new Program("Новости".toCharArray());

        Channel channel = new Channel("Первый канал".toCharArray(), 1);

        channel.addProgram(program1);
        channel.addProgram(program2);
        channel.addProgram(program3);

        Program program4 = new Program("Передача".toCharArray());
        Channel channel2 = new Channel("Россия".toCharArray(), 2);
        channel2.addProgram(program4);

        Program program0 = new Program("Инфо".toCharArray());
        Channel channel0 = new Channel("Инфо канал".toCharArray(), 0);
        channel0.addProgram(program0);

        TV tv = new TV("SAMSUNG".toCharArray());
        tv.addChannel(channel);
        tv.addChannel(channel2);
        tv.addChannel(channel0);

        RemoteController remoteController = new RemoteController(tv);
        tv.remoteController = remoteController;

        //Текущая передача, которая идет на канале
        channel.setСurrentProgram(0);
        channel0.setСurrentProgram(0);
        channel2.setСurrentProgram(0);

        remoteController.on(random.nextInt(2));

        Channel included = tv.getIncluded();
        if (included != null) {
            System.out.println("Установлен канал: " + included.getNumber());
            String name = new String(included.getCurrentProgram().getName());
            System.out.println("А на нем сейчас идет передача: " + name);
        }

    }
}
