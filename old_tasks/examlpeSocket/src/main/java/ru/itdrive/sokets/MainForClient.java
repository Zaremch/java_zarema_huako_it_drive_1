package ru.itdrive.sokets;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        SocketClient client = new SocketClient("127.0.0.1", 7777);

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}
