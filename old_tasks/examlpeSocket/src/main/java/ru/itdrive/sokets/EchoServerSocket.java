package ru.itdrive.sokets;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServerSocket {

    private class ReceiverMessageTaskClient extends Thread {

        private Socket socketClient;

        public ReceiverMessageTaskClient(Socket socketClient) {
            this.socketClient = socketClient;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    System.out.printf("На сервере запущен фоновый процесс: %s \n", Thread.currentThread().getName());

                    InputStream clientInputStream = socketClient.getInputStream();
                    BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));

                    String inputLine;
                    while (true) {

                        inputLine = clientReader.readLine();
                        PrintWriter writer = new PrintWriter(socketClient.getOutputStream(), true);
                        writer.println(inputLine);

                        System.out.println(inputLine);

                    }

                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };

    public void start(int port) {

        ServerSocket serverSocket;

        try {

            serverSocket = new ServerSocket(port);

            while (true) {
                Socket socketClient = serverSocket.accept();
                ReceiverMessageTaskClient receiverMessageTaskClient = new ReceiverMessageTaskClient(socketClient);
                receiverMessageTaskClient.start();
            }

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

    }
}
