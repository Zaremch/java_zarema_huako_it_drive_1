package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner Scanner = new Scanner(System.in);

        int listGrade1 = 0;
        int listGrade2 = 0;

        //В int помещается только 9 цифр. тут можно ставить меньше для теста
        int maxCountQueue = 9;

        String week;
        int grade, minGrade, numberWeek;
        int previousWeek = 0;
        int countWeek = 0;
        while (true) {

            if (countWeek > 18) {
                break;
            }

            System.out.println("Введите неделю");
            week = Scanner.next();

            if (week.equals("42")) {
                break;
            }
            countWeek++;

            numberWeek = numberWeek(week);
            if ((numberWeek == -1) || !(numberWeek == (previousWeek + 1))) {
                throw new IllegalArgumentException("неделя введена некорректно");
            }
            previousWeek = numberWeek;

            minGrade = -1;

            for (int i = 0; i < 5; i++) {
                System.out.println("Введите оценку");
                grade = Scanner.nextInt();
                //Проверка
                if (minGrade == -1) {
                    minGrade = grade;
                } else if (grade < minGrade) {
                    minGrade = grade;
                }
            }

            if (countWeek <= maxCountQueue) {
                listGrade1 = listGrade1 * 10 + minGrade;
            } else {
                listGrade2 = listGrade2 * 10 + minGrade;
            }

        }

        //нужна очередь цифр, а не стэк((
        int currentCountWeek = 0;
        int del;
        int currentQueue;
        numberWeek = 0;
        for (int i = 1; i <= 2; i++) {

            currentCountWeek = Math.min(countWeek, maxCountQueue);
            del = (int) Math.pow(10, currentCountWeek-1);

            if (i==1) {
                currentQueue = listGrade1;
            } else {
                currentQueue = listGrade2;
            }

            while (del > 0) {

                grade = currentQueue / del;

                numberWeek++;
                printGrade(grade, numberWeek);

                currentQueue = currentQueue % del;
                del = del / 10;
            }

            countWeek = countWeek - currentCountWeek;
        }

    }

    public static void printGrade(int grade, int numberWeek) {

        System.out.printf("Week %d ", numberWeek);
        for (int i = 1; i <= grade; i++) {
            System.out.print("=");
        }
        System.out.println(">");
    }

    public static int numberWeek(String week) {
        char[] arrayWeek = week.toCharArray();

        if (!(arrayWeek[0] == 'W' && arrayWeek[1] == 'e' && arrayWeek[2] == 'e' && arrayWeek[3] == 'k')) {
            return -1;
        }

        int sum = 0;
        int temp = 0;
        for (int i = 4; i < arrayWeek.length; i++) {
            if (isDigit(arrayWeek[i]) == false) {
                return -1;
            }

            temp = toInt(arrayWeek[i]);
            sum = sum * 10 + temp;
        }
        return sum;
    }

    public static boolean isDigit(char character) {
        return (character >= '0' & character <= '9');
    }

    public static int toInt(char character) {
        return character - '0';
    }
}
