package com.company;

public class Main {

    public static void main(String[] args) {

        //Реализовать функцию сравнения строк
        char[] a = {'a', 'a', 'a'};
        char[] b = {'a', 'b'};
        char[] c = {'a', 'b', 'a'};
        System.out.println(stringCompare(a, b));
        System.out.println(stringCompare(b, a));
        System.out.println(stringCompare(b, c));

        //Реализовать функцию, которая принимает на вход массив символов-цифр, а возвращает тип int
        char[] arrayСharacter = {'2', '3', '2', '1'};
        System.out.println(toInt(arrayСharacter));

        //Реализовать функцию, которая выводит наиболее часто встречающиеся буквы в тексте (первую тройку),
        //если количество букв совпадает - вывести первую тройку в алфавитном порядке.
        System.out.println(frequentCharacters("AСйцукенггшВВВВВ"));
        System.out.println(frequentCharacters("После отъезда государя из Москвы московская жизнь потекла прежним, обычным порядком, и течение этой жизни было так обычно, что трудно было вспомнить о бывших днях патриотического восторга и увлечения, и трудно было верить, что действительно Россия в опасности и что члены Английского клуба суть вместе с тем и сыны отечества, готовые для него на всякую жертву. Одно, что напоминало о бывшем во время пребывания государя в Москве общем восторженно-патриотическом настроении, было требование пожертвований людьми и деньгами, которые, как скоро они были сделаны, облеклись в законную, официальную форму и казались неизбежны."));

    }

    public static String frequentCharacters(String text) {

        text = text.toUpperCase();

        //33+26 всего букв англ и русских
        int[] arrayCount = new int[58];
        char[] arrayCharacters = new char[58];

        char i = 'A'; //английсская
        int indexArray = 0;
        while (i <= 'Я') {
            arrayCharacters[indexArray] = i;
            if (i == 'Z') {
                i = 'А'; //русская
            } else {
                i++;
            }
            indexArray++;
        }

        char сurrentСharacter;
        for (int y = 0; y < text.length(); y++) {
            сurrentСharacter = text.charAt(y);

            if (сurrentСharacter >= 'A' && сurrentСharacter <= 'Z') {
                arrayCount[сurrentСharacter - 65]++;
            } else if (сurrentСharacter >= 'А' && сurrentСharacter <= 'Я') {
                arrayCount[сurrentСharacter - 1014]++;
            }
        }

        int iMax;
        int temp;
        char tempChar;
        for (int x = 0; x < 3; x++) {
            iMax = x;
            for (int j = x + 1; j < arrayCount.length; j++) {
                if (arrayCount[j] > arrayCount[iMax]) {
                    iMax = j;
                } else if (arrayCount[j] == arrayCount[iMax] & arrayCharacters[j] < arrayCharacters[iMax]) {
                    iMax = j;
                }
            }

            if (iMax != x) {
                temp = arrayCount[x];
                arrayCount[x] = arrayCount[iMax];
                arrayCount[iMax] = temp;

                tempChar = arrayCharacters[x];
                arrayCharacters[x] = arrayCharacters[iMax];
                arrayCharacters[iMax] = tempChar;

            }
        }

        return Character.toString(arrayCharacters[0]) + Character.toString(arrayCharacters[1]) + Character.toString(arrayCharacters[2]);
    }

    public static int stringCompare(char[] arrayСharacter1, char[] arrayСharacter2) {

        int lengthArrayСharacter1 = arrayСharacter1.length;
        int lengthArrayСharacter2 = arrayСharacter2.length;

        int minlength = Math.min(lengthArrayСharacter1, lengthArrayСharacter2);
        int result = 0;
        if (lengthArrayСharacter1 < lengthArrayСharacter2) {
            result = -1;
        } else if (lengthArrayСharacter1 > lengthArrayСharacter2) {
            result = 1;
        }

        for (int i = 0; i < minlength; i++) {
            if (arrayСharacter1[i] < arrayСharacter2[i]) {
                result = -1;
                break;
            }

            if (arrayСharacter1[i] > arrayСharacter2[i]) {
                result = 1;
                break;
            }
        }

        return result;
    }

    public static int toInt(char[] arrayСharacter) {

        int sum = 0;
        int length = arrayСharacter.length;
        int digit;

        for (int i=0; i < length; i++) {

            if (isDigit(arrayСharacter[i]) == false) {
                return -1;
            }

            digit = arrayСharacter[i] - '0';
            sum = sum * 10 + digit;

        }
        return sum;
    }

    public static boolean isDigit(char character) {
        return (character >= '0' & character <= '9');
    }

}
