package com.company;

public class Human {
    private String name;
    private int age;

    public void setAge(int age) {
        if (age<0) {
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    Human (String name, int age) {
        setName(name);
        setAge(age);
    }

}
