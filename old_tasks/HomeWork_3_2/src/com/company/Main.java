package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[100];

        int age;
        String name;
        for (int i = 0; i < humans.length; i++) {

            age = random.nextInt(150);
            name = randomName();

            humans[i] = new Human(name, age);
            
            System.out.println(name + " " + age);
        }

        int jMax;
        Human tempHuman;
        for (int i = 0; i < humans.length - 1; i++) {

            jMax = i;
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[jMax].getAge() < humans[j].getAge()) {
                    jMax = j;
                }
            }

            if (jMax != i) {
                tempHuman = humans[jMax];
                humans[jMax] = humans[i];
                humans[i] = tempHuman;
            }
        }

        int maxCountAge = 1; //максимальное количество
        int countAge = 1; //текущее количество
        int currentAge = humans[0].getAge(); //текущий возраст
        int maxAge = currentAge;//возраст, который чаще встречается
        for (int i = 1; i < humans.length; i++) {

            if (humans[i].getAge() == currentAge) {
                countAge++;
            } else {
                countAge = 1;
                currentAge = humans[i].getAge();
            }

            //может быть последний элемент.
            if (maxCountAge < countAge) {
                maxCountAge = countAge;
                maxAge = humans[i].getAge();
            }

        }

        System.out.println("Чаще всего встречается возраст: " + maxAge);

    }

    public static String randomName() {
        Random random = new Random();
        int lengtnName = random.nextInt(20);

        int min = 65;
        int max = 90;
        int diff = max - min;

        int indexChar;
        String randomName = "";
        for (int i = 0; i < lengtnName; i++) {
            indexChar = random.nextInt(diff + 1) + min;
            randomName = randomName + (char) indexChar;
        }

        return randomName;
    }

}
