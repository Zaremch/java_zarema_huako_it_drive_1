package com.company;

import jdk.nashorn.internal.ir.IfNode;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String[] arrayStudents = fillStudents();
        int[][] arrayLessons = fillLessons();
        int[][][] fillAttendance = fillAttendance(arrayStudents);

        // Поэтому, вы должны реализовать MVP-версию проекта только для этого месяца:)
        //format.parse пользоваться нелья, но знаем что сентябрь 2020
        int dayWeek;//вторник
        for (int iStudent = -1; iStudent < arrayStudents.length; iStudent++) {

            if (iStudent == -1) {
                System.out.printf("%10s", "");
            } else if (arrayStudents[iStudent] == null) {
                continue;
            } else {
                System.out.println();
                System.out.printf("%-10s", arrayStudents[iStudent]);
            }

            dayWeek = 1;//вторник
            for (int day = 1; day <= 30; day++) {

                if (dayWeek == 6) {
                    dayWeek = 0;
                    continue;
                }

                for (int y = 0; y < arrayLessons[dayWeek].length; y++) {

                    //Нет занятий в графике
                    if (arrayLessons[dayWeek][y] == 0) {
                        continue;
                    }

                    //Шапка таблицы
                    if (iStudent == -1) {
                        System.out.printf("%d:00 %s %2d|", y + 1, DayWeek(dayWeek), day);
                    } else if (fillAttendance[iStudent][day - 1][y] == 0) {
                        System.out.printf("%10s|", "");
                    } else {
                        System.out.printf("%10d|", fillAttendance[iStudent][day - 1][y]);
                    }

                }//Обход времен графика

                dayWeek++;

            }//Обход дней месяцы

        }//Обход студентов
    }

    public static int indexStudent(String[] arrayStudents, String student) {

        for (int i = 0; i < arrayStudents.length; i++) {
            if (arrayStudents[i].equals(student)) {
                return i;
            }
        }

        return -1;
    }

    public static int[][][] fillAttendance(String[] arrayStudents) {

        int[][][] arrayAttendance = new int[10][31][6];

        Scanner scanner = new Scanner(System.in);
        String student;
        int indexStudent;
        int numberLessonTime, dayMonth;
        String result;
        while (true) {
            student = scanner.next();
            if (student.equals(".")) {
                break;
            }

            indexStudent = indexStudent(arrayStudents, student);
            if (indexStudent == -1) {
                System.out.println("Нет такого студента. Попробуй еще");
                continue;
            }

            numberLessonTime = scanner.nextInt();
            if (numberLessonTime > 6) {
                throw new IllegalArgumentException("Время введено некорректно");
            }

            dayMonth = scanner.nextInt();

            if (dayMonth < 1 & dayMonth > 31) {
                throw new IllegalArgumentException("День месяца введено некорректно");
            }

            result = scanner.next();
            if (result.equals("HERE")) {
                arrayAttendance[indexStudent][dayMonth - 1][numberLessonTime - 1] = 1;
            } else if (result.equals("NOT_HERE")) {
                arrayAttendance[indexStudent][dayMonth - 1][numberLessonTime - 1] = -1;
            }

        }

        return arrayAttendance;
    }

    public static int[][] fillLessons() {

        Scanner scanner = new Scanner(System.in);

        int[][] arrayLessons = new int[6][6];
        String lessonTime;
        String dayWeek;
        int numberDayWeek, numberLessonTime;
        int countLessons = 0;

        while (true) {

            lessonTime = scanner.next();
            if (lessonTime.equals(".")) {
                break;
            }

            //charAt тоже нельзя исползовать вроде. Ну первый элемент массива:)
            numberLessonTime = toInt(lessonTime.charAt(0));
            if (numberLessonTime == -1) {
                throw new IllegalArgumentException("Время занятия введено некорректно");
            }

            dayWeek = scanner.next();
            numberDayWeek = numberDayWeek(dayWeek);
            if (numberDayWeek == -1) {
                throw new IllegalArgumentException("День недели введен некорректно");
            }

            //Если введено время 2 часа, то храним в индексе 1, т.к. есть 0
            if (arrayLessons[numberDayWeek][numberLessonTime - 1] != 1) {
                countLessons++;
            }

            arrayLessons[numberDayWeek][numberLessonTime - 1] = 1;
            if (countLessons == 10) {
                break;
            }
        }

        return arrayLessons;
    }

    public static String[] fillStudents() {

        String[] arrayStudents = new String[10];
        Scanner scanner = new Scanner(System.in);

        String student;
        for (int i = 0; i < arrayStudents.length; i++) {
            student = scanner.next();
            if (student.equals(".")) {
                break;
            }

            if (correctName(student) == false) {
                throw new IllegalArgumentException("Имя студента введено некорректно");
            }

            arrayStudents[i] = student;
        }
        return arrayStudents;
    }

    public static boolean correctName(String student) {
        //lengtn string использовать нельзя по условию. Вроде
        char[] arrayCharName = student.toCharArray();
        for (int i = 0; i < arrayCharName.length; i++) {

            if (i > 9) {
                return false;
            }

        }
        return true;
    }

    public static String DayWeek(int numberDayWeek) {
        String[] arrayDayWeek = {"MO", "TU", "WE", "TH", "FR", "SA"};
        return arrayDayWeek[numberDayWeek];
    }

    public static int numberDayWeek(String dayWeek) {

        String[] arrayDayWeek = {"MO", "TU", "WE", "TH", "FR", "SA"};
        for (int i = 0; i < arrayDayWeek.length; i++) {
            if (arrayDayWeek[i].equals(dayWeek)) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isDigit(char character) {
        return (character >= '0' & character <= '9');
    }

    public static int toInt(char character) {

        if (isDigit(character) == false) {
            return -1;
        }

        return character - '0';
    }
}
