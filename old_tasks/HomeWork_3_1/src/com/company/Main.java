package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        char lines[][] = new char[4][];
        for (int i = 0; i < lines.length; i++) {
            lines[i] = scanner.nextLine().toCharArray();
        }

        int iMin;
        char[] tempArray;
        for (int i = 0; i < lines.length - 1; i++) {

            iMin = i;
            for (int j = i + 1; j < lines.length; j++) {
                if (stringCompare(lines[iMin], lines[j]) == 1) {
                    iMin = j;
                }
            }

            tempArray = lines[i];
            lines[i] = lines[iMin];
            lines[iMin] = tempArray;

        }

        for (int i = 0; i < lines.length; i++) {
            for (int j = 0; j < lines[i].length; j++) {
                System.out.print(lines[i][j] + " ");
            }
            System.out.println();
        }

    }

    public static int stringCompare(char[] arrayСharacter1, char[] arrayСharacter2) {

        int lengthArrayСharacter1 = arrayСharacter1.length;
        int lengthArrayСharacter2 = arrayСharacter2.length;

        int minlength = Math.min(lengthArrayСharacter1, lengthArrayСharacter2);
        int result = 0;
        if (lengthArrayСharacter1 < lengthArrayСharacter2) {
            result = -1;
        } else if (lengthArrayСharacter1 > lengthArrayСharacter2) {
            result = 1;
        }

        for (int i = 0; i < minlength; i++) {
            if (arrayСharacter1[i] < arrayСharacter2[i]) {
                result = -1;
                break;
            }

            if (arrayСharacter1[i] > arrayСharacter2[i]) {
                result = 1;
                break;
            }
        }

        return result;
    }
}