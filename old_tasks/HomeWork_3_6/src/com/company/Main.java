package com.company;

public class Main {

    public static void main(String[] args) {

        IntegersArrayList list = new IntegersArrayList();
        list.add(5, list.getCount());
        list.add(2, list.getCount());
        list.add(15, list.getCount());
        list.add(7, list.getCount());
        list.add(2, 0);
        list.add(3, 4);

        IntegersArrayList.ArrayListIterator iterator = list.new ArrayListIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        list.remove(2);
        iterator.getStart();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        list.reverse();
        iterator.getStart();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        System.out.println("--------------------------------------------------------");

        IntegerLinkedList linkedList = new IntegerLinkedList();
        linkedList.add(6, linkedList.getCount());
        linkedList.add(2, linkedList.getCount());
        linkedList.add(3, linkedList.getCount());
        linkedList.add(1, linkedList.getCount());
        linkedList.add(4, 2);
        linkedList.add(8, 0);
        linkedList.add(1, 1);
        linkedList.add(6, linkedList.getCount());
        linkedList.add(7, linkedList.getCount() - 1);

        IntegerLinkedList.ArrayListIterator iteratorList = linkedList.new ArrayListIterator();
        while (iteratorList.hasNext()) {
            System.out.print(iteratorList.next() + " ");
        }
        System.out.println();

        linkedList.remove(0);
        iteratorList.getStart();
        while (iteratorList.hasNext()) {
            System.out.print(iteratorList.next() + " ");
        }
        System.out.println();

        linkedList.remove(linkedList.getCount() - 1);
        iteratorList.getStart();
        while (iteratorList.hasNext()) {
            System.out.print(iteratorList.next() + " ");
        }
        System.out.println();

        linkedList.remove(2);
        iteratorList.getStart();
        while (iteratorList.hasNext()) {
            System.out.print(iteratorList.next() + " ");
        }
        System.out.println();

        linkedList.reverse();
        iteratorList.getStart();
        while (iteratorList.hasNext()) {
            System.out.print(iteratorList.next() + " ");
        }

    }

}
