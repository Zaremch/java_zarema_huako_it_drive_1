package com.company;

public class IntegersArrayList {
    private int[] elements;
    private static final int DEFAULT_ARRAY_SIZE = 4;
    private int count;

    class ArrayListIterator {
        private int current;

        ArrayListIterator() {
            current = 0;
        }

        void getStart() {
            current = 0;
        }

        public boolean hasNext() {
            return current < count;
        }

        public int next() {
            int nextElement = elements[current];
            current++;
            return  nextElement;
        }
    }

    public IntegersArrayList() {
        count = 0;
        elements = new int[DEFAULT_ARRAY_SIZE];
    }

    public void add(int element, int index) {

        if (!(index >= 0 && index <= count)) {
            System.err.println("Array out of bounds");
            return;
        }

        if (count == elements.length) {
            changeLangtnArray((int) (count * 1.5));
        }

        if (index == count) {
            elements[count] = element;
        } else {
            int[] elementsCopy = new int[elements.length];
            for (int i = 0; i < count; i++) {
                if (i < index) {
                    elementsCopy[i] = elements[i];
                } else if (i == index) {
                    elementsCopy[i] = element;
                    elementsCopy[i + 1] = elements[i];
                } else {
                    elementsCopy[i + 1] = elements[i];
                }
            }
            elements = elementsCopy;
        }

        count++;
    }

    public void remove(int index) {

        if (!(index >= 0 && index < count)) {
            System.err.println("Array out of bounds");
            return;
        }

        int[] elementsCopy = new int[elements.length];
        for (int i = 0; i < count; i++) {
            if (i < index) {
                elementsCopy[i] = elements[i];
            } else if (i > index) {
                elementsCopy[i - 1] = elements[i];
            }
        }
        elements = elementsCopy;

        count--;
    }

    public void reverse() {
        int lengtn = count / 2;
        int tempElement;
        for (int i = 0; i < lengtn; i++) {
            tempElement = elements[i];
            elements[i] = elements[count - 1 - i];
            elements[count - 1 - i] = tempElement;
        }
    }

    public int getCount() {
        return count;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Index out of range");
            return -1;
        }
    }

    private void changeLangtnArray(int newLengtn) {
        int[] elementsCopy = new int[newLengtn];
        //Если массив обрезаем
        int length = Math.min(newLengtn, elements.length);
        for (int i = 0; i < length; i++) {
            elementsCopy[i] = elements[i];
        }
        elements = elementsCopy;
        count = Math.min(count, newLengtn);
    }

}
