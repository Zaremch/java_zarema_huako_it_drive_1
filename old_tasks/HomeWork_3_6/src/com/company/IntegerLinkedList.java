package com.company;

import com.sun.deploy.security.SelectableSecurityManager;

public class IntegerLinkedList {
    private Node first;
    private Node last;
    private int count;

    class ArrayListIterator {
        private Node current;

        ArrayListIterator() {
            current = first;
        }

        void getStart() {
            current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            int nextElement = current.value;
            current = current.next;
            return nextElement;
        }
    }

    private static class Node {
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
            ;
        }
    }

    public IntegerLinkedList() {
        count = 0;
    }

    public void add(int element, int index) {

        if (!(index >= 0 && index <= count)) {
            System.err.println("Array out of bounds");
            return;
        }

        Node newNode = new Node(element);
        if (count == 0) {
            first = newNode;
            last = newNode;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
        } else if (index == count) {
            last.next = newNode;
            last = newNode;
        } else {
            Node current = getNote(index - 1);
            Node currentNext = current.next;

            current.next = newNode;
            newNode.next = currentNext;
        }
        count++;

    }

    public Node getFirst() {
        return first;
    }

    public int getCount() {
        return count;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return getNote(index).value;
        } else {
            System.err.println("Index out of range");
            return -1;
        }
    }

    public void remove(int index) {
        if (!(index >= 0 && index < count)) {
            System.err.println("Array out of bounds");
            return;
        }
        if (index == 0) {
            first = getNote(1);
        }
        if (index == count - 1) {
            last = getNote(index - 1);
            //если в списке вдруг один элемент
            if (last != null) {
                last.next = null;
            }
        }
        if (index > 0 && index < count - 1) {
            Node current = getNote(index - 1);
            Node currentNext = getNote(index + 1);
            current.next = currentNext;
        }
        count--;
    }

    private Node getNote(int index) {

        if (!(index >= 0 && index < count)) {
            return null;
        }

        Node current = first;
        for (int i = 1; i <= index; i++) {
            current = current.next;
        }
        return current;
    }

    public void reverse() {

        if (count < 2) {
            return;
        }

        Node startPair = first;
        Node endPair = first.next;
        startPair.next = null;

        Node current;
        while (endPair != null) {
            current = endPair.next;
            endPair.next = startPair;

            startPair = endPair;
            endPair = current;
        }

        Node tempNode = first;
        first = last;
        last = tempNode;
    }
}
