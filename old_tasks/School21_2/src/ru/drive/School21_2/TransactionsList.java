package ru.drive.School21_2;

public interface TransactionsList {
    public void addTransaction(Transaction transaction);
    public void deleteTransaction(String UUID);
    public Transaction[] toArray();
}
