package ru.drive.School21_2;
import jdk.nashorn.internal.ir.IfNode;
public class UsersArrayList implements UsersList {

    private User[] users;
    private static final int COUNT_USERS = 10;
    private int count = 0;

    public UsersArrayList() {
        this.users = new User[COUNT_USERS];
        this.count = 0;
    }

    public void addUser(User user) {
        if (count == users.length) {
            changeLangtnArray((int) (count * 1.5));
        }

        users[count] = user;
        count++;
    }

    public User getUserID(int id) {

        for (int i = 0; i < count; i++) {
            if (id == users[i].getId()) {
                return users[i];
            }
        }

        throw new IllegalArgumentException("UserNotFoundException");
    }

    public User getUserIndex(int index) {
        if (index >= 0 && index < count) {
            return users[index];
        }

        return null;
    }

    public int getCountUsers() {
        return count;
    }

    private void changeLangtnArray(int newLengtn) {
        User[] elementsCopy = new User[newLengtn];

        //Если массив обрезаем
        int length = Math.min(newLengtn, users.length);
        for (int i = 0; i < length; i++) {
            elementsCopy[i] = users[i];
        }

        users = elementsCopy;
        count = Math.min(count, newLengtn);
    }
}
