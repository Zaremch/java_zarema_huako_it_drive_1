package ru.drive.School21_2;

import javax.jws.soap.SOAPBinding;
import java.util.Scanner;
import java.lang.IllegalAccessException;


public class Program {

    public static TransactionsService transactionsService;
    public static Scanner scanner;

    public static void main(String[] args) {

        scanner = new Scanner(System.in);
        transactionsService = new TransactionsService();

        int i = -1;
        while (i != 7) {

            printInstruction();
            i = scanner.nextInt();

            try
            {
                if (i == 1) {
                    addUser();
                } else if (i == 2) {
                    printBalanceUser();
                } else if (i == 3) {
                    addTransaction();
                } else if (i == 4) {
                    printTransactionsUser();
                } else if (i == 5) {
                    deleteTransaction();
                } else if (i == 6) {
                    CheckTransactions();
                }
            }
            catch(Exception ex)
            {
                System.out.println("\n" + ex.toString());
            }

            System.out.println("_______________________________________");
        }
    }

    public static void CheckTransactions() {

        System.out.println("Результаты проверки:");

        Transaction[] transactions = transactionsService.transactionNoPair();
        User userSender;
        User userReceiver;
        for (Transaction variable : transactions) {
            userReceiver = variable.getReceiver();
            userSender = variable.getSender();
            System.out.printf("To %s (id = %d) имеет неподтвержденный перевод id = %s от %s(id = %d) на сумму %.2f", userReceiver.getName(), userReceiver.getId(), variable.getUUID(), userSender.getName(), userSender.getId(), variable.getSum());
            System.out.println();
        }
    }

    public static void deleteTransaction() {

        System.out.println("Введите id пользователя и id перевода");

        int id = scanner.nextInt();
        String uuid = scanner.next();

        User user = transactionsService.getUserID(id);
        String name = user.getName();

        float sum = user.getTransactions().getTransaction(uuid).getSum();
        transactionsService.deleteTransactions(id, uuid);

        System.out.printf("Перевод To %s (id = %d) %.2f удален", name, id, sum);
        System.out.println();

    }

    public static void printTransactionsUser() {

        System.out.println("Введите id пользователя");
        int id = scanner.nextInt();
        User user = transactionsService.getUserID(id);

        //1. можно ли так делать?
        //Или нужно в идеале вынести обход в transactionsService.
        //Мы же отсюда не должны знать как хранятся транзакци у юзеров
        //Или как? Я немного запуталась(

        //2. Получается 2 раза обходим коллекцию
        //тут и еще внутри, чтобы получить массив
        //но ведь для чего-то нужен был изначально массив?
        //или нужно было получить список и его обходить?

        String name = user.getName();

        Transaction[] transactions = user.getTransactions().toArray();
        for (Transaction variable : transactions) {
            System.out.printf("To %s (id = %d) %.2f with id = %s", user.getName(), id, variable.getSum(), variable.getUUID());
            System.out.println();
        }
    }

    public static void addTransaction() {
        System.out.println("Введите id-отправителя, id-получается и сумму перевода");
        int idSender = scanner.nextInt();
        int idReceiver = scanner.nextInt();
        float sum = scanner.nextFloat();

        transactionsService.transferMoney(idReceiver, idSender, sum);
        System.out.println("Перевод осуществлен");
    }

    public static void printBalanceUser() {

        System.out.println("Введите id пользователя");
        int id = scanner.nextInt();
        User user = transactionsService.getUserID(id);
        if (user == null) {
            return;
        }
        float balance = transactionsService.getBalance(user);

        System.out.printf("%s - %.2f", user.getName(), balance);
        System.out.println();

    }

    public static void addUser() {
        System.out.println("Введите имя и баланс пользователя");

        String name = scanner.next();
        float balance = scanner.nextFloat();

        User user = new User(name, balance);
        transactionsService.addUser(user);

        System.out.printf("Пользователь добавлен с id = %d", user.getId());
        System.out.println();
    }

    public static void test() {

        User user1 = new User("Джоли", 100);
        User user2 = new User("Брэд", 10);
        User user3 = new User("Еще кто-то", -1);

        Transaction transaction1 = new Transaction("123", user1, user2, true);
        transaction1.setSum(10);

        Transaction transaction2 = new Transaction("124", user1, user2, true);
        transaction1.setSum(10);

        int s = UserIdsGenerator.getInstance().generateId();

        System.out.println();
        UsersArrayList usersArrayList = new UsersArrayList();
        usersArrayList.addUser(user1);
        usersArrayList.addUser(user2);
        usersArrayList.addUser(user3);
        System.out.printf("Количество пользователей: %d", usersArrayList.getCountUsers());
        System.out.println();
        User userTest = usersArrayList.getUserID(1);
        userTest = usersArrayList.getUserIndex(0);

    }

    public static void printInstruction() {

        System.out.println("1. Добавить пользователя");
        System.out.println("2. Посмотреть баланс пользователей");
        System.out.println("3. Осуществить перевод");
        System.out.println("4. Посмотреть все переводы конкретного пользователя");
        System.out.println("5. DEV - удалить перевод по ID");
        System.out.println("6. DEV - проверить корректность переводов");
        System.out.println("7. Завершить выполнение");
    }

}
