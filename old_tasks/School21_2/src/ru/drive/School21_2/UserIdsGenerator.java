package ru.drive.School21_2;

public class UserIdsGenerator {

    private int numerator;
    private static UserIdsGenerator instance;

    private UserIdsGenerator() {

    }

    public int generateId() {
        return numerator++;
    }

    public static UserIdsGenerator getInstance() {
        if (instance == null) {
            instance = new UserIdsGenerator();
        }
        return instance;
    }

}

