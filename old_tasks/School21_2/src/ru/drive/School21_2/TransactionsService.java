package ru.drive.School21_2;

import java.util.UUID;

public class TransactionsService {
    private UsersArrayList usersList;
    private TransactionsLinkedList transactionsList;

    public TransactionsService() {
        this.usersList = new UsersArrayList();
        this.transactionsList = new TransactionsLinkedList();
    }

    public void addUser(User user) {
        usersList.addUser(user);
    }

    //В условии было так написано
    //А разве не лучше вообще спрятать user-a
    //Из Program (клиент) просто передавать id, а на выходе получать имя пользователя, его переводы и т.д.
    //Чтобы клиент вообще не знал,
    //что на свете есть какие-то userы и как они хранятся?
    public User getUserID(int id) {
        return usersList.getUserID(id);
    }

    public float getBalance(User user) {
        return user.getBalance();
    }

    public void transferMoney(int idReceiver, int idSender, float sum) {
        User userReceiver = usersList.getUserID(idReceiver);
        User userSender = usersList.getUserID(idSender);

        String uuid = "" + UUID.randomUUID();

        TransactionsList transactionsListReceiver = userReceiver.getTransactions();
        TransactionsList transactionsListSender = userSender.getTransactions();

        //<<Начать транзакцию
        //Отправка или расход
        Transaction transactionSender = new Transaction(uuid, userReceiver, userSender, false);
        transactionSender.setSum(-sum);
        transactionsListSender.addTransaction(transactionSender);
        userSender.setBalance(-sum);

        //IllegalTransactionException???

        //Все транзакции. Ибо не надо цикла в цикле и в цикле))
        //И в БД была бы отдельная таблица
        transactionsList.addTransaction(transactionSender);
        //>>Закончить транзакцию

        //Приход
        Transaction transactionReceiver = new Transaction(uuid, userReceiver, userSender, true);
        transactionReceiver.setSum(sum);
        transactionsListReceiver.addTransaction(transactionReceiver);
        userReceiver.setBalance(sum);

        transactionsList.addTransaction(transactionReceiver);
    }

    public Transaction[] userTransactions(User user) {
        return user.getTransactions().toArray();
    }

    public void deleteTransactions(int idUser, String uuidTransaction) {
        User user = usersList.getUserID(idUser);

        Transaction transaction = user.getTransactions().getTransaction(uuidTransaction);

        transactionsList.deleteTransactionRef(transaction);
        user.getTransactions().deleteTransaction(uuidTransaction);
    }

    public Transaction[] transactionNoPair() {
        Transaction[] allTransactions = transactionsList.toArray();
        TransactionsList transactionNoPairList = new TransactionsLinkedList();

        int length = allTransactions.length;
        boolean foundTransaction;
        String uuidСurrent;
        for (int i = 0; i < length; i++) {
            foundTransaction = false;
            uuidСurrent = allTransactions[i].getUUID();
            for (int j = 0; j < length; j++) {

                if (allTransactions[i] == allTransactions[j]) {
                    continue;
                }

                if (uuidСurrent == allTransactions[j].getUUID()) {
                    foundTransaction = true;
                    break;
                }
            }
            if (!foundTransaction) {
                transactionNoPairList.addTransaction(allTransactions[i]);
            }
        }
        return transactionNoPairList.toArray();
    }
}

