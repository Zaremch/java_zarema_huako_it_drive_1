package ru.drive.School21_2;
import java.util.LinkedList;

public class TransactionsLinkedList implements TransactionsList {

    private LinkedList<Transaction> listTransaction;
    private int count = 0;

    public TransactionsLinkedList() {
        this.listTransaction = new LinkedList<Transaction>();
        this.count = 0;
    }

    public void addTransaction(Transaction transaction) {
        listTransaction.add(transaction);
        count++;
    }

    public Transaction getTransaction(String UUID) {
        for (Transaction variable : listTransaction) {
            if (variable.getUUID().equals(UUID)) {
                return variable;
            }
        }

        return null;
    }

    public void deleteTransactionRef(Transaction transaction) {
        for (Transaction variable : listTransaction) {
            if (variable == transaction) {
                listTransaction.remove(variable);
                count--;
                return;
            }
        }
    }

    public void deleteTransaction(String UUID) {
        for (Transaction variable : listTransaction) {
            if (variable.getUUID().equals(UUID)) {
                listTransaction.remove(variable);
                count--;
                return;
            }
        }
        throw new IllegalArgumentException("TransactionNotFoundException");
    }

    public Transaction[] toArray() {
        Transaction[] arrayTransaction = new Transaction[count];
        int i = 0;
        for (Transaction variable : listTransaction) {
            arrayTransaction[i] = variable;
            i++;
        }
        return arrayTransaction;
    }

}
