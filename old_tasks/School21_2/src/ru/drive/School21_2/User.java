package ru.drive.School21_2;

import jdk.nashorn.internal.ir.IfNode;

public class User {
    private int id;
    private String name;
    private float balance;
    private TransactionsLinkedList transactions;

    User(String name, float balance) {
        this.id = UserIdsGenerator.getInstance().generateId();
        this.name = name;

        if (balance < 0) {
            System.err.printf("Начальный баланс не может быть отрицательным. Для пользователя %s будет установлен баланс 0", name);
            this.balance = 0;
        } else {
            this.balance = balance;
        }
        this.transactions = new TransactionsLinkedList();
    }

    public TransactionsLinkedList getTransactions() {
        return transactions;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getBalance() {
        return balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(float balance) {

        float sumBalance = this.balance + balance;

        if (sumBalance < 0) {
            throw new IllegalArgumentException("IllegalTransactionException");
        }

        this.balance = sumBalance;
    }
}
