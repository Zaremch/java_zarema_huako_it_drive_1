package ru.drive.School21_2;

public class Transaction {
    private String UUID;
    private User receiver;
    private User sender;
    private float sum;
    private boolean isComing; //это приход

    public Transaction(String UUID, User receiver, User sender, boolean isComing) {
        this.UUID = UUID;
        this.receiver = receiver;
        this.sender = sender;
        this.isComing = isComing;
    }

    public void setSum(float sum) {
        if (isComing && sum < 0) {
            System.err.println("Сумма прихода не может быть отрицательной");
            return;
        }
        if (!isComing && sum > 0) {
            System.err.println("Сумма расхода не может быть положительной");
            return;
        }
        this.sum = sum;
    }

    public String getUUID() {
        return UUID;
    }

    public float getSum() {
        return sum;
    }

    public User getReceiver() {
        return receiver;
    }

    public User getSender() {
        return sender;
    }
}
