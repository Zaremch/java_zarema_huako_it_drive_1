package ru.drive.School21_2;

public interface UsersList {
    public void addUser(User user);
    User getUserID(int id);
    User getUserIndex(int index);
    int getCountUsers();
}
