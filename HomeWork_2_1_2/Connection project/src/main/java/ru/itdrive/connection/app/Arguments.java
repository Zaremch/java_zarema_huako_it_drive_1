package ru.itdrive.connection.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {
	
	@Parameter(names = {"--fileName"})
	public String fileName;

}