package ru.itdrive.ChatServer.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component (value = "messageRepositoryJdbcImpl")
public class MessageRepositoryJdbcImpl implements MessageRepository {

    private DataSource dataSource;

    public MessageRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Message save(Message message) {

        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {

            Connection connection = this.dataSource.getConnection();

            //language=SQL
            String sql_text = "insert into message(text, date_of_post, room_id, author_id)\n" +
                    "values (?, ?, ?, ?);";

            statement = connection.prepareStatement(sql_text, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, message.getText());
            statement.setTimestamp(2, new java.sql.Timestamp(message.getDateOfPost().getTime()));
            statement.setInt(3, message.getRoom().getId());
            statement.setInt(4, message.getUser().getId());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new IllegalStateException("Message save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {

                Integer id = generatedKeys.getInt("id");
                message.setId(id);

            } else {
                throw new SQLException("Something wrong:(");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (generatedKeys != null) {
                try {
                    generatedKeys.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return message;
    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Message object) {

    }

    @Override
    public List<Message> findAllLastMessages(int limit, Room room) {

        //language=SQL
        String sql_text = "SELECT message.id,\n" +
                "       message.date_of_post,\n" +
                "       message.text,\n" +
                "       message.room_id,\n" +
                "       r.title,\n" +
                "       message.author_id,\n" +
                "       u.nickname\n" +
                "from message\n" +
                "         inner join users u on u.id = message.author_id\n" +
                "         inner join room r on r.id = message.room_id\n" +
                "where room_id = %room_id%\n" +
                "order by date_of_post \n" +
                "limit %limit%;";

        sql_text = sql_text.replace("%limit%", "" + limit);
        sql_text = sql_text.replace("%room_id%", "" + room.getId());

        Statement statement = null;
        List<Message> result = new ArrayList<Message>();
        ResultSet resultSet = null;

        try {

            Connection connection = this.dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql_text);

            while (resultSet.next()) {

                User user = User.builder()
                        .id(resultSet.getInt("author_id"))
                        .nickname(resultSet.getString("nickname").trim())
                        .build();

                Message message = Message.builder()
                        .id(resultSet.getInt("id"))
                        .text(resultSet.getString("text").trim())
                        .dateOfPost(resultSet.getDate("date_of_post"))
                        .room(room)
                        .user(user)
                        .build();

                result.add(message);

            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {

                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {

                }
            }
        }

        return result;

    }

    @Override
    public Message find(Integer id) {
        return null;
    }

    @Override
    public List<Message> findALL() {
        return null;
    }
}
