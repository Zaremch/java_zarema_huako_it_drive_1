package ru.itdrive.ChatServer.app;

import com.beust.jcommander.JCommander;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.ChatServer.config.ApplicationConfig;
import ru.itdrive.ChatServer.services.SocketServer;
import ru.itdrive.ChatServer.services.SocketServerImpl;

import javax.sql.DataSource;

public class MainServer {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        //ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SocketServer service = context.getBean(SocketServer.class);
        service.start(7777);

    }
}
