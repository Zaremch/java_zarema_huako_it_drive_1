package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;

import java.util.List;

public interface MessageRepository extends СrudRepository<Message> {
    List<Message> findAllLastMessages(int limit, Room room);
}
