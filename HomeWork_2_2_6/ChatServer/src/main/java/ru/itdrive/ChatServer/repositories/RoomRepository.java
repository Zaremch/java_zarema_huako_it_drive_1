package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

public interface RoomRepository extends СrudRepository<Room> {
    Room findByRoom(Room room);
    Room findByLastUserVisit(User user);
}
