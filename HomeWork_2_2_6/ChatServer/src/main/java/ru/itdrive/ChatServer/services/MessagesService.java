package ru.itdrive.ChatServer.services;

import ru.itdrive.ChatServer.models.Message;

import java.util.List;

public interface MessagesService {
    Integer addUser(String nickname);
    Integer addRoom(String title);
    Message sendMessage(Integer idUser, Integer idRoom, String message);
    List<Message> findAllLastMessages(int limit, Integer idRoom);
    void saveToRoom(Integer idUser, Integer idRoom);
    Integer findByLastUserVisit(Integer idUser);
}
