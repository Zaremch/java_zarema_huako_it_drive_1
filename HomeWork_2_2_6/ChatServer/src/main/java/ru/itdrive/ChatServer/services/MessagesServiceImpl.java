package ru.itdrive.ChatServer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.itdrive.ChatServer.models.Message;
import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;
import ru.itdrive.ChatServer.repositories.*;

import java.util.Date;
import java.util.List;

@Component
public class MessagesServiceImpl implements MessagesService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    @Qualifier("messageRepositoryJdbcImpl")
    private MessageRepository messageRepository;

    @Override
    public Integer addUser(String nickname) {

        if (nickname.equals("")) {
            nickname = "Аноним";
        }

        User user = User.builder()
                .nickname(nickname)
                .id(-1)
                .build();

        user = userRepository.findByUser(user);

        if (user.getId() == -1) {
            user = userRepository.save(user);
        }

        return user.getId();

    }

    @Override
    public Integer addRoom(String title) {

        Room room = Room.builder()
                .title(title)
                .id(-1)
                .build();

        room = roomRepository.findByRoom(room);
        if (room.getId() == -1) {
            room = roomRepository.save(room);
        }

        return room.getId();

    }

    @Override
    public Message sendMessage(Integer idUser, Integer idRoom, String text) {

        User user = userRepository.find(idUser);
        if (user == null) {
            return null;
        }

        Room room = roomRepository.find(idRoom);
        if (room == null) {
            return null;
        }

        Message message = Message.builder()
                .text(text)
                .room(room)
                .dateOfPost(new Date())
                .user(user)
                .build();

        message = messageRepository.save(message);

        return message;
    }

    @Override
    public List<Message> findAllLastMessages(int limit, Integer idRoom) {

        Room room = roomRepository.find(idRoom);
        List<Message> lastMessages = messageRepository.findAllLastMessages(limit, room);

        return lastMessages;

    }

    @Override
    public void saveToRoom(Integer idUser, Integer idRoom) {

        User user = userRepository.find(idUser);
        if (user == null) {
            return;
        }

        Room room = roomRepository.find(idRoom);
        if (room == null) {
            return;
        }

        userRepository.saveToRoom(user, room);

    }

    @Override
    public Integer findByLastUserVisit(Integer idUser) {

        User user = userRepository.find(idUser);
        if (user == null) {
            return -1;
        }

        Room room = roomRepository.findByLastUserVisit(user);
        if (room == null) {
            return -1;
        } else {
            return room.getId();
        }
    }
}

