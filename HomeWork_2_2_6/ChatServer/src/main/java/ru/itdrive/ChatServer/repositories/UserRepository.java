package ru.itdrive.ChatServer.repositories;

import ru.itdrive.ChatServer.models.Room;
import ru.itdrive.ChatServer.models.User;

public interface UserRepository extends СrudRepository<User> {
    User findByUser(User user);
    void saveToRoom(User user, Room room);
}
