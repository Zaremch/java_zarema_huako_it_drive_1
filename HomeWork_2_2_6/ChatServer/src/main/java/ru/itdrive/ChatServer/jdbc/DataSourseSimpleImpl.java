package ru.itdrive.ChatServer.jdbc;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class DataSourseSimpleImpl implements DataSource {

    private Connection connection;
    private String url;
    private String user;
    private String password;
    private String driveClassName;

    public DataSourseSimpleImpl(String url, String user, String password, String driveClassName) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.driveClassName = driveClassName;
    }

    @Override
    public Connection getConnection() throws SQLException {

        if (this.connection != null && !this.connection.isClosed()) {
            return this.connection;
        }

        try {
            //Не работает:(((((((((((((((((((
            //Class.forName(this.driveClassName);
            this.connection = DriverManager.getConnection(this.url, this.user, this.password);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

        return this.connection;

    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
